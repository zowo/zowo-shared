import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

// We need exactly kotlin 1.3.50 because that is what native immutable collections lib (v0.3) is built against.

val artifactName = "zowo-shared-jvm"
val artifactGroup = "app.zowo"
val artifactVersion = "0.0.2-SNAPSHOT"

val pomUrl = "https://gitlab.com/zowo/zowo-shared"
val pomScmUrl = "https://gitlab.com/zowo/zowo-shared"
val pomIssueUrl = "https://gitlab.com/zowo/zowo-shared"
val pomDesc = "Zowo shared code for JVM"

val pomLicenseName = "COMMERCIAL"
val pomLicenseUrl = "https://zowo.app"
val pomLicenseDist = "repo"

buildscript {
  repositories {
    mavenCentral()
  }

  dependencies {
    classpath(kotlin("gradle-plugin", version = "1.3.50"))
  }
}

repositories {
  mavenCentral()
  // Kotlin immutable collections
  jcenter()
}

plugins {
  kotlin("multiplatform") version "1.3.50"
  id("maven-publish")
  id("com.jfrog.bintray") version "1.8.4"
}

kotlin {
  jvm {
    val main by compilations.getting {
      kotlinOptions {
        // Setup the Kotlin compiler options for the 'main' compilation:
        jvmTarget = "1.8"
      }

      compileKotlinTask // get the Kotlin task 'compileKotlinJvm'
      output // get the main compilation output
    }

    mavenPublication {
      groupId = artifactGroup
      artifactId = artifactName
      version = artifactVersion

      pom.withXml {
        asNode().apply {
          appendNode("description", pomDesc)
          appendNode("name", rootProject.name)
          appendNode("url", pomUrl)
          appendNode("licenses").appendNode("license").apply {
            appendNode("name", pomLicenseName)
            appendNode("url", pomLicenseUrl)
            appendNode("distribution", pomLicenseDist)
          }
          appendNode("scm").apply {
            appendNode("url", pomScmUrl)
          }
        }
      }
    }
  }

  //select iOS target platform depending on the Xcode environment variables
  val iOSTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget =
    if (System.getenv("SDK_NAME")?.startsWith("iphoneos") == true)
      ::iosArm64
    else
      ::iosX64

  iOSTarget("ios") {
    binaries {
      framework {
        baseName = "Zowo"
      }
    }
  }

  sourceSets {
    all {
      languageSettings.apply {
        enableLanguageFeature("InlineClasses")
      }
    }

    val commonMain by getting {
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-common")
        api("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.3")
      }
      languageSettings.apply {
        languageVersion = "1.3" // possible values: '1.0', '1.1', '1.2', '1.3'
        apiVersion = "1.3" // possible values: '1.0', '1.1', '1.2', '1.3'
      }
    }

    val commonTest by getting {
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-test-common")
        implementation("org.jetbrains.kotlin:kotlin-test-annotations-common")
      }
    }

    val jvmMain by getting {
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib")
      }
    }

    val jvmTest by getting {
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-test")
        implementation("org.jetbrains.kotlin:kotlin-test-junit")
//                implementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
//                runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
      }
    }

    val iosMain by getting {
      dependencies {
      }
    }
  }
}

val packForXcode by tasks.creating(Sync::class) {
  val targetDir = File(buildDir, "xcode-frameworks")

  /// selecting the right configuration for the iOS
  /// framework depending on the environment
  /// variables set by Xcode build
  val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
  val framework = kotlin.targets
    .getByName<KotlinNativeTarget>("ios")
    .binaries.getFramework(mode)
  inputs.property("mode", mode)
  dependsOn(framework.linkTask)

  from({ framework.outputDirectory })
  into(targetDir)

  /// generate a helpful ./gradlew wrapper with embedded Java path
  doLast {
    val gradlew = File(targetDir, "gradlew")
    gradlew.writeText("#!/bin/bash\n"
      + "export 'JAVA_HOME=${System.getProperty("java.home")}'\n"
      + "cd '${rootProject.rootDir}'\n"
      + "./gradlew \$@\n")
    gradlew.setExecutable(true)
  }
}

bintray {
  // Getting bintray user and key from properties file or command line
  user = if (project.hasProperty("bintray_user")) project.property("bintray_user") as String else ""
  key = if (project.hasProperty("bintray_key")) project.property("bintray_key") as String else ""

  // Automatic publication enabled
  publish = true

  // Set maven publication onto bintray plugin
  setPublications("jvm")

  // Configure package
  pkg.apply {
    repo = "zowo-shared"
    name = artifactName
    userOrg = "zowo"
    setLicenses("Apache-2.0")
    setLabels("Kotlin", "JVM", "Zowo")
    vcsUrl = pomScmUrl
    websiteUrl = pomUrl
    issueTrackerUrl = pomIssueUrl

    // Configure version
    version.apply {
      name = artifactVersion
      desc = pomDesc
    }
  }
}

tasks.getByName("build").dependsOn(packForXcode)

tasks.withType(GenerateModuleMetadata::class) {
  enabled = false
}