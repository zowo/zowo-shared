fun Int.times(f: () -> Unit) {
  for (idx in 0 until this) {
    f()
  }
}