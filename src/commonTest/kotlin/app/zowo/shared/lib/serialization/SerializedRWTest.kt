package app.zowo.shared.lib.serialization

import kotlinx.collections.immutable.persistentHashMapOf
import kotlinx.collections.immutable.persistentListOf
import times
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZNone
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.lib.functional.ZSome
import app.zowo.shared.lib.serialization.SerializedRW.persistentListRW
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalStdlibApi
internal class SerializedRWTest {
  val garbagePre = ByteRope.create("random".toCharArray().map { it.toByte() }.toByteArray())
  val garbagePost = ByteRope.create("garbage".toCharArray().map { it.toByte() }.toByteArray())

  fun <A, B> test(
    serializer: ISerializedRW<A>,
    deserializer: IDeserializer<B>,
    value: A, expectedValue: B
  ) {
    val data = (garbagePre + serializer.serialize(value) + garbagePost).toArray()
//        println("Serialized: ${data.decodeToString()}")
//        println("Serialized: ${data.toList()}")
    val bytesExpected = data.size - garbagePre.size - garbagePost.size
    assertEquals(
      ZRight(DeserializeInfo(expectedValue, bytesExpected)),
      deserializer.deserialize(data, garbagePre.size)
    )
  }

  fun <A> testSame(
    serializer: ISerializedRW<A>,
    deserializer: IDeserializer<A>,
    value: A, expectedValue: A? = null
  ) = test(serializer, deserializer, value, expectedValue ?: value)

  fun <A> testRW(rw: ISerializedRW<A>, value: A) = test(rw, rw, value, value)

  @Test
  fun testByteMin() = testRW(SerializedRW.byte, Byte.MIN_VALUE)

  @Test
  fun testByteMax() = testRW(SerializedRW.byte, Byte.MAX_VALUE)

  @Test
  fun testByte1() = testRW(SerializedRW.byte, 16)

  @Test
  fun testByte2() = testRW(SerializedRW.byte, 67)

  @Test
  fun testByteRand() = 100.times {
    testRW(SerializedRW.byte, Random.nextInt(Byte.MIN_VALUE.toInt(), Byte.MAX_VALUE + 1).toByte())
  }

  @Test
  fun testBoolTrue() = testRW(SerializedRW.bool, true)

  @Test
  fun testBoolFalse() = testRW(SerializedRW.bool, false)

  @Test
  fun testShort0() = testRW(SerializedRW.short, 0)

  @Test
  fun testShortMin() = testRW(SerializedRW.short, Short.MIN_VALUE)

  @Test
  fun testShortHalfMin() = testRW(SerializedRW.short, (Short.MIN_VALUE / 2).toShort())

  @Test
  fun testShortHalfMax() = testRW(SerializedRW.short, (Short.MAX_VALUE / 2).toShort())

  @Test
  fun testShortMax() = testRW(SerializedRW.short, Short.MAX_VALUE)

  @Test
  fun testShortRand() = 100.times {
    testRW(SerializedRW.short, Random.nextInt(Short.MIN_VALUE.toInt(), Short.MAX_VALUE + 1).toShort())
  }

  @Test
  fun testInt0() = testRW(SerializedRW.int, 0)

  @Test
  fun testIntMin() = testRW(SerializedRW.int, Int.MIN_VALUE)

  @Test
  fun testIntHalfMin() = testRW(SerializedRW.int, Int.MIN_VALUE / 2)

  @Test
  fun testIntHalfMax() = testRW(SerializedRW.int, Int.MAX_VALUE / 2)

  @Test
  fun testIntMax() = testRW(SerializedRW.int, Int.MAX_VALUE)

  @Test
  fun testIntRand() = 100.times { testRW(SerializedRW.int, Random.nextInt(Int.MIN_VALUE, Int.MAX_VALUE)) }

  @Test
  fun testLong0() = testRW(SerializedRW.long, 0)

  @Test
  fun testLongMin() = testRW(SerializedRW.long, Long.MIN_VALUE)

  @Test
  fun testLongHalfMin() = testRW(SerializedRW.long, Long.MIN_VALUE / 2)

  @Test
  fun testLongHalfMax() = testRW(SerializedRW.long, Long.MAX_VALUE / 2)

  @Test
  fun testLongMax() = testRW(SerializedRW.long, Long.MAX_VALUE)

  @Test
  fun testLongRand() = 100.times { testRW(SerializedRW.long, Random.nextLong(Long.MIN_VALUE, Long.MAX_VALUE)) }

  @Test
  fun testStringEmpty() = testRW(SerializedRW.string, "")

  @Test
  fun testString1() = testRW(SerializedRW.string, "!")

  @Test
  fun testString2() = testRW(SerializedRW.string, "bad")

  @Test
  fun testString3() = testRW(SerializedRW.string, "strings")

  @Test
  fun testStringUTF8() = testRW(SerializedRW.string, "ąčęėįšųūž€ĄČĘĖĮŠŲŪŽ")

  @Test
  fun testAnd2() = testRW(SerializedRW.pair(SerializedRW.int, SerializedRW.string), Pair(42, "meaning"))

  @Test
  fun testListEmpty() = testRW(persistentListRW(SerializedRW.int), persistentListOf())

  @Test
  fun testListNonEmpty() = testRW(persistentListRW(SerializedRW.int), persistentListOf(5, 6, 7, 2))

  @Test
  fun testDiscriminatedA() = testRW(OneOf.rw, OneOf.A)

  @Test
  fun testDiscriminatedB1() = testRW(OneOf.rw, OneOf.B(0))

  @Test
  fun testDiscriminatedB2() = testRW(OneOf.rw, OneOf.B(42))

  @Test
  fun taggedShouldWorkRw1_1() = testRW(Tagged.rw, Tagged(ZNone, ZNone))

  @Test
  fun taggedShouldWorkRw1_2() = testRW(Tagged.rw, Tagged(ZSome(42), ZNone))

  @Test
  fun taggedShouldWorkRw1_3() = testRW(Tagged.rw, Tagged(ZSome(42), ZSome("reason")))

  @Test
  fun taggedShouldWorkRw1_4() = testRW(Tagged.rw, Tagged(ZNone, ZSome("reason")))

  @Test
  fun taggedShouldWorkRw2_1() = testRW(Tagged.intAs0, Tagged(ZNone, ZNone))

  @Test
  fun taggedShouldWorkRw2_2() = testRW(Tagged.intAs0, Tagged(ZSome(42), ZNone))

  @Test
  fun taggedShouldWorkRw2_3() = testRW(Tagged.intAs0, Tagged(ZSome(42), ZSome("reason")))

  @Test
  fun taggedShouldWorkRw2_4() = testRW(Tagged.intAs0, Tagged(ZNone, ZSome("reason")))

  @Test
  fun taggedShouldWorkRw3_1() = testRW(Tagged.intAs2, Tagged(ZNone, ZNone))

  @Test
  fun taggedShouldWorkRw3_2() = testRW(Tagged.intAs2, Tagged(ZSome(42), ZNone))

  @Test
  fun taggedShouldWorkRw3_3() = testRW(Tagged.intAs2, Tagged(ZSome(42), ZSome("reason")))

  @Test
  fun taggedShouldWorkRw3_4() = testRW(Tagged.intAs2, Tagged(ZNone, ZSome("reason")))

  @Test
  fun taggedShouldWorkRw4_1() = testRW(Tagged.strAs0, Tagged(ZNone, ZNone))

  @Test
  fun taggedShouldWorkRw4_2() = testRW(Tagged.strAs0, Tagged(ZSome(42), ZNone))

  @Test
  fun taggedShouldWorkRw4_3() = testRW(Tagged.strAs0, Tagged(ZSome(42), ZSome("reason")))

  @Test
  fun taggedShouldWorkRw4_4() = testRW(Tagged.strAs0, Tagged(ZNone, ZSome("reason")))

  @Test
  fun taggedShouldWorkRw5_1() = testRW(Tagged.strAs2, Tagged(ZNone, ZNone))

  @Test
  fun taggedShouldWorkRw5_2() = testRW(Tagged.strAs2, Tagged(ZSome(42), ZNone))

  @Test
  fun taggedShouldWorkRw5_3() = testRW(Tagged.strAs2, Tagged(ZSome(42), ZSome("reason")))

  @Test
  fun taggedShouldWorkRw5_4() = testRW(Tagged.strAs2, Tagged(ZNone, ZSome("reason")))

  @Test
  fun taggedShouldWorkRw6_1() = testRW(Tagged2.strDefault, Tagged2(ZNone, "reason"))

  @Test
  fun taggedShouldWorkRw6_2() = testRW(Tagged2.strDefault, Tagged2(ZSome(42), "reason"))

  @Test
  fun taggedShouldFillNoneWhereOldDataCanNotBeFound() = testSame(
    serializer = Tagged.intAs0, deserializer = Tagged.intAs2,
    value = Tagged(ZNone, ZSome("reason"))
  )

  @Test
  fun taggedShouldFillNoneWhereOldDataIsUnderDifferentTag() = test(
    serializer = Tagged.intAs0, deserializer = Tagged.intAs2,
    value = Tagged(ZSome(42), ZSome("reason")),
    expectedValue = Tagged(ZNone, ZSome("reason"))
  )

  @Test
  fun taggedShouldFillDefaultWhereOldDataCanNotBeFound() = test(
    serializer = Tagged.strAs0, deserializer = Tagged2.strDefault,
    value = Tagged(ZNone, ZNone),
    expectedValue = Tagged2(ZNone, "default")
  )

  data class Tagged(val i: ZOption<Int>, val s: ZOption<String>) {
    companion object {
      val rw = TaggedRW2(
        0, SerializedRW.int.opt().failIfMissing(),
        1, SerializedRW.string.opt().failIfMissing(),
        { a1, a2 -> Tagged(a1, a2) }, { it.i }, { it.s }
      )
      val intAs0 = TaggedRW2(
        0, SerializedRW.int.noneIfMissing(),
        1, SerializedRW.string.opt().failIfMissing(),
        { a1, a2 -> Tagged(a1, a2) }, { it.i }, { it.s }
      )
      val intAs2 = TaggedRW2(
        2, SerializedRW.int.noneIfMissing(),
        1, SerializedRW.string.opt().failIfMissing(),
        { a1, a2 -> Tagged(a1, a2) }, { it.i }, { it.s }
      )
      val strAs0 = TaggedRW2(
        1, SerializedRW.int.noneIfMissing(),
        0, SerializedRW.string.noneIfMissing(),
        { a1, a2 -> Tagged(a1, a2) }, { it.i }, { it.s }
      )
      val strAs2 = TaggedRW2(
        1, SerializedRW.int.noneIfMissing(),
        0, SerializedRW.string.noneIfMissing(),
        { a1, a2 -> Tagged(a1, a2) }, { it.i }, { it.s }
      )
    }
  }

  data class Tagged2(val i: ZOption<Int>, val s: String) {
    companion object {
      val strDefault = TaggedRW2(
        1, SerializedRW.int.noneIfMissing(),
        0, SerializedRW.string.defaultIfMissing("default"),
        { a1, a2 -> Tagged2(a1, a2) }, { it.i }, { it.s }
      )
    }
  }

  sealed class OneOf {
    object A : OneOf()
    data class B(val i: Int) : OneOf() {
      companion object {
        val rw = SerializedRW.int.inmap({ B(it) }, { it.i })
      }
    }

    companion object {
      val rw = DiscriminatedRW(
        {
          when (it) {
            A -> ByteRope.create(0)
            is B -> ByteRope.create(1) + B.rw.serialize(it)
          }
        },
        persistentHashMapOf(
          0.toByte() to Deserializer.const(A),
          1.toByte() to B.rw
        )
      )
    }
  }
}