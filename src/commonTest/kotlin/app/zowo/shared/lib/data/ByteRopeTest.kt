package app.zowo.shared.lib.data

import kotlin.test.Test
import kotlin.test.assertEquals

internal class ByteRopeTest {
  val b1 = ByteRope.create(1)
  val b2 = ByteRope.create(1, 2)
  val b3 = ByteRope.create(1, 2, 3)
  val b4 = ByteRope.create(1, 2, 3, 4)
  val b8 = ByteRope.create(1, 2, 3, 4, 5, 6, 7, 8)

  @Test
  fun toArray() {
    assertEquals(listOf<Byte>(1), b1.toArray().toList())
    assertEquals(listOf<Byte>(1, 2), b2.toArray().toList())
    assertEquals(listOf<Byte>(1, 2, 3), b3.toArray().toList())
    assertEquals(listOf<Byte>(1, 2, 3, 4), b4.toArray().toList())
    assertEquals(listOf<Byte>(1, 2, 3, 4, 5, 6, 7, 8), b8.toArray().toList())
    assertEquals(listOf<Byte>(1, 1, 2), (b1 + b2).toArray().toList())
    assertEquals(
      listOf<Byte>(
        1,
        1, 2,
        1, 2, 3,
        1, 2, 3, 4,
        1, 2, 3, 4, 5, 6, 7, 8
      ), (b1 + b2 + b3 + b4 + b8).toArray().toList()
    )
  }

  @Test
  fun getSize() {
    assertEquals(0, ByteRope.Empty.size)
    assertEquals(1, b1.size)
    assertEquals(2, b2.size)
    assertEquals(3, b3.size)
    assertEquals(4, b4.size)
    assertEquals(8, b8.size)
    assertEquals(8 + 3, (b8 + b3).size)
    assertEquals(8 + 3, (b8 + b3 + ByteRope.Empty).size)
  }
}