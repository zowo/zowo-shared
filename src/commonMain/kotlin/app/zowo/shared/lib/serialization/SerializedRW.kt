package app.zowo.shared.lib.serialization

import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.exts.validIndex
import app.zowo.shared.lib.exts.validIndexes
import app.zowo.shared.lib.functional.ZLeft
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.lib.functional.flatMap

object SerializedRW {
  // Mask for first byte
  const val ByteMask = 0b11111111
  const val ByteMaskL: Long = 0b11111111

  @Suppress("NOTHING_TO_INLINE")
  inline infix fun Long.extractByte(byteIdx: Int): Byte =
    ((this ushr (8 * byteIdx)) and ByteMaskL).toByte()

  @Suppress("NOTHING_TO_INLINE")
  inline infix fun Int.extractByte(byteIdx: Int): Byte =
    ((this ushr (8 * byteIdx)) and ByteMask).toByte()

  @ExperimentalUnsignedTypes
  @Suppress("NOTHING_TO_INLINE")
  inline fun shortFromBytes(b1: Byte, b0: Byte): Short =
    ((b1.toUByte().toUInt() shl 8) or b0.toUByte().toUInt()).toShort()

  @ExperimentalUnsignedTypes
  @Suppress("NOTHING_TO_INLINE")
  inline fun intFromBytes(b3: Byte, b2: Byte, b1: Byte, b0: Byte): Int = (
    (b3.toUByte().toUInt() shl (8 * 3))
      .or((b2.toUByte().toUInt() shl (8 * 2)))
      .or(shortFromBytes(b1, b0).toUShort().toUInt())
    ).toInt()

  @ExperimentalUnsignedTypes
  @Suppress("NOTHING_TO_INLINE")
  inline fun longFromBytes(b7: Byte, b6: Byte, b5: Byte, b4: Byte, b3: Byte, b2: Byte, b1: Byte, b0: Byte): Long = (
    (b7.toUByte().toULong() shl (8 * 7))
      .or((b6.toUByte().toULong() shl (8 * 6)))
      .or((b5.toUByte().toULong() shl (8 * 5)))
      .or((b4.toUByte().toULong() shl (8 * 4)))
      .or(intFromBytes(b3, b2, b1, b0).toUInt().toULong())
    ).toLong()

  object byte : SerializedRWFuncs<Byte>(
    { ByteRope.create(it) },
    { data, idx ->
      if (data.validIndex(idx)) ZRight(DeserializeInfo(data[idx], 1))
      else ZLeft("Unknown data index $idx. Size=${data.size}")
    }
  )

  val ubyte = byte.inmap({ it.toUByte() }, { it.toByte() })

  val bool = byte.inmapE(
    {
      val falseByte: Byte = 0
      val trueByte: Byte = 1
      when (it) {
        falseByte -> ZRight(false)
        trueByte -> ZRight(true)
        else -> ZLeft("Unknown value '$it'")
      }
    },
    { if (it) 1 else 0 }
  )

  object short : SerializedRWFuncs<Short>(
    {
      val int = it.toInt()
      ByteRope.create(int extractByte 1, int extractByte 0)
    },
    { data, idx ->
      if (data.validIndexes(idx, idx + 1))
        ZRight(DeserializeInfo(shortFromBytes(data[idx], data[idx + 1]), 2))
      else ZLeft("Invalid indexes ${idx}..${idx + 1}, size=${data.size}")
    }
  )

  object int : SerializedRWFuncs<Int>(
    { ByteRope.create(it extractByte 3, it extractByte 2, it extractByte 1, it extractByte 0) },
    { data, idx ->
      if (data.validIndexes(idx, idx + 3)) ZRight(DeserializeInfo(
        intFromBytes(data[idx], data[idx + 1], data[idx + 2], data[idx + 3]), 4
      ))
      else ZLeft("Invalid indexes ${idx}..${idx + 3}, size=${data.size}")
    }
  )

  object long : SerializedRWFuncs<Long>(
    {
      ByteRope.create(
        it extractByte 7, it extractByte 6, it extractByte 5, it extractByte 4,
        it extractByte 3, it extractByte 2, it extractByte 1, it extractByte 0
      )
    },
    { data, idx ->
      if (data.validIndexes(idx, idx + 7)) ZRight(DeserializeInfo(longFromBytes(
        data[idx], data[idx + 1], data[idx + 2], data[idx + 3],
        data[idx + 4], data[idx + 5], data[idx + 6], data[idx + 7]
      ), 8))
      else ZLeft("Invalid indexes ${idx}..${idx + 7}, size=${data.size}")
    }
  )

  @ExperimentalStdlibApi
  object string : SerializedRWFuncs<String>(
    {
      val bytes = ByteRope.create(it.encodeToByteArray())
      int.serialize(bytes.size) + bytes
    },
    { data, idx ->
      int.deserialize(data, idx).flatMap { lenDi ->
        if (lenDi.value == 0) ZRight(DeserializeInfo("", lenDi.bytesRead))
        else {
          val strStart = idx + lenDi.bytesRead
          val strEnd = idx + lenDi.bytesRead + lenDi.value
          if (data.validIndexes(strStart, strEnd - 1)) ZRight(DeserializeInfo(
            data.decodeToString(strStart, strEnd), lenDi.bytesRead + lenDi.value
          ))
          else ZLeft("Invalid indexes ${strStart}..${strEnd}, size=${data.size}")
        }
      }
    }
  )

  fun <A, B> pair(aRw: ISerializedRW<A>, bRw: ISerializedRW<B>): ISerializedRW<Pair<A, B>> =
    AndRw2(aRw, bRw, { a, b -> Pair(a, b) }, { it.first }, { it.second })

  fun <A> persistentListRW(rw: ISerializedRW<A>): ISerializedRW<PersistentList<A>> =
    CombinedRW(CollectionSerializer(rw), CollectionDeserializer.persistentList(rw))

  fun <A> persistentSetRW(rw: ISerializedRW<A>): ISerializedRW<PersistentSet<A>> =
    CombinedRW(CollectionSerializer(rw), CollectionDeserializer.persistentSet(rw))

  fun <K, V> persistentMapRW(kRw: ISerializedRW<K>, vRw: ISerializedRW<V>): ISerializedRW<PersistentMap<K, V>> =
    CombinedRW(
      CollectionSerializer(Serializer.mapEntry(kRw, vRw)).contramap { it.entries },
      CollectionDeserializer.persistentMap(kRw, vRw)
    )
}