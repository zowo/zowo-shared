package app.zowo.shared.lib.serialization

import kotlinx.collections.immutable.PersistentMap
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.exts.validIndex
import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZLeft

class DiscriminatedRW<A>(
  private val serializeFunc: (A) -> ByteRope,
  val byDiscriminator: PersistentMap<Byte, IDeserializer<A>>
) : ISerializedRW<A> {
  override fun serialize(a: A): ByteRope = serializeFunc(a)

  override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> {
    if (!serialized.validIndex(startIndex)) return ZLeft("Unknown index $startIndex. Size=${serialized.size}")

    val discriminator = serialized[startIndex]
    return when (val deserializer = byDiscriminator[discriminator]) {
      null -> ZLeft("Unknown discriminator at $startIndex: '$discriminator'")
      else -> deserializer.deserialize(serialized, startIndex + 1).map { it.addBytes(1) }
    }
  }
}