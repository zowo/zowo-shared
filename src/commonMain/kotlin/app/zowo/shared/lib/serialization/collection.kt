package app.zowo.shared.lib.serialization

import kotlinx.collections.immutable.*
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZLeft
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.lib.functional.flatMap

class CollectionSerializer<A>(val serializer: ISerializer<A>) : ISerializer<Collection<A>> {
  override fun serialize(a: Collection<A>): ByteRope =
    a.fold(SerializedRW.int.serialize(a.size), { rope, elem -> rope + serializer.serialize(elem) })
}

interface CollectionBuilder<C, A> {
  fun add(a: A)
  fun build(): C
}

object CollectionBuilders {
  fun <A> persistentList(): CollectionBuilder<PersistentList<A>, A> =
    object : CollectionBuilder<PersistentList<A>, A> {
      val builder = persistentListOf<A>().builder()
      override fun add(a: A) {
        builder.add(a)
      }

      override fun build(): PersistentList<A> = builder.build()
    }

  fun <A> persistentSet(): CollectionBuilder<PersistentSet<A>, A> =
    object : CollectionBuilder<PersistentSet<A>, A> {
      val builder = persistentSetOf<A>().builder()
      override fun add(a: A) {
        builder.add(a)
      }

      override fun build(): PersistentSet<A> = builder.build()
    }

  fun <K, V> persistentMap(): CollectionBuilder<PersistentMap<K, V>, Map.Entry<K, V>> =
    object : CollectionBuilder<PersistentMap<K, V>, Map.Entry<K, V>> {
      val builder = persistentHashMapOf<K, V>().builder()
      override fun add(a: Map.Entry<K, V>) {
        builder[a.key] = a.value
      }

      override fun build(): PersistentMap<K, V> = builder.build()
    }
}

class CollectionDeserializer<C, A>(
  val createBuilder: (Int) -> CollectionBuilder<C, A>,
  val deserializer: IDeserializer<A>
) : IDeserializer<C> {
  override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<C>> =
    SerializedRW.int.deserialize(serialized, startIndex).flatMap { countDi ->
      if (countDi.value < 0) ZLeft("Data says collection should be of ${countDi.value} elements!")
      else {
        val builder = createBuilder(countDi.value)
        var position = startIndex + countDi.bytesRead
        for (idx in 0 until countDi.value) {
          when (val result = deserializer.deserialize(serialized, position)) {
            is ZLeft -> return@flatMap ZLeft("Deserializing $idx at $position failed: ${result.get}")
            is ZRight -> {
              builder.add(result.get.value)
              position += result.get.bytesRead
            }
          }
        }

        ZRight(DeserializeInfo(builder.build(), position - startIndex))
      }
    }

  companion object {
    fun <A> persistentList(deserializer: IDeserializer<A>) = CollectionDeserializer<PersistentList<A>, A>(
      { _ -> CollectionBuilders.persistentList() }, deserializer
    )

    fun <A> persistentSet(deserializer: IDeserializer<A>) = CollectionDeserializer<PersistentSet<A>, A>(
      { _ -> CollectionBuilders.persistentSet() }, deserializer
    )

    fun <K, V> persistentMap(
      kDeserializer: IDeserializer<K>, vDeserializer: IDeserializer<V>
    ) = CollectionDeserializer<PersistentMap<K, V>, Map.Entry<K, V>>(
      { _ -> CollectionBuilders.persistentMap() }, Deserializer.mapEntry(kDeserializer, vDeserializer)
    )
  }
}