package app.zowo.shared.lib.serialization

import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.data.Cell
import app.zowo.shared.lib.functional.*

interface IDeserializerForTagged<A> : IDeserializer<A> {
  val defaultValue: A?
}

interface ISerializedRWForTagged<A> : IDeserializerForTagged<A>, ISerializer<A>

fun <A> ISerializedRW<A>.noneIfMissing(): ISerializedRWForTagged<ZOption<A>> {
  val self = this
  return object : ISerializedRWForTagged<ZOption<A>> {
    override fun serialize(a: ZOption<A>): ByteRope = when (a) {
      is ZSome -> self.serialize(a.get)
      is ZNone -> ByteRope.Empty
    }

    override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<ZOption<A>>> =
      self.deserialize(serialized, startIndex).map { it.map<ZOption<A>> { ZSome(it) } }

    override val defaultValue: ZOption<A>?
      get() = ZNone
  }
}

fun <A> ISerializedRW<A>.failIfMissing(): ISerializedRWForTagged<A> {
  val self = this
  return object : ISerializedRWForTagged<A> {
    override fun serialize(a: A): ByteRope = self.serialize(a)

    override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> =
      self.deserialize(serialized, startIndex)

    override val defaultValue: A?
      get() = null
  }
}

fun <A> ISerializedRW<A>.defaultIfMissing(default: A): ISerializedRWForTagged<A> {
  val self = this
  return object : ISerializedRWForTagged<A> {
    override fun serialize(a: A): ByteRope = self.serialize(a)

    override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> =
      self.deserialize(serialized, startIndex)

    override val defaultValue: A?
      get() = default
  }
}

object TaggedSerializer {
  fun tagged(coll: Collection<Pair<Byte, ByteRope>>): ByteRope {
    // Do not write empty fields.
    val woEmpty = coll.filter { !it.second.isEmpty }
    return woEmpty.fold(
      // [entry count]
      SerializedRW.ubyte.serialize(woEmpty.size.toUByte())
    ) { acc, pair ->
      // [previous] + [data length] + [tag] + [data]
      ByteRope.Single(acc + SerializedRW.int.serialize(pair.second.size), pair.first) + pair.second
    }
  }
}

abstract class TaggedRW<A, Temp> : ISerializedRW<A> {
  abstract fun serializeToCollection(a: A): Collection<Pair<Byte, ByteRope>>
  override fun serialize(a: A): ByteRope = TaggedSerializer.tagged(serializeToCollection(a))

  override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> {
    val entries = serialized[startIndex].toUByte().toInt()
//        println("Entries=$entries")
    var idx = startIndex + 1
    val temp = init()

    for (entryIdx in 0 until entries) {
//            println("Entry=$entryIdx @ $idx")
      val length = when (val l = SerializedRW.int.deserialize(serialized, idx)) {
        is ZRight -> {
          idx += l.get.bytesRead
          l.get.value
        }
        is ZLeft -> return ZLeft("Error reading entry $entryIdx, can't read frame length: ${l.get}")
      }
//            println("length=$length")
//            println("tag=${serialized[idx]} @ $idx")
      idx += when (val result = tryRead(serialized, idx, temp)) {
        null -> {
//                    println("Unknown tag ${serialized[idx]}, skipping")
          1 + length
        }
        is ZRight -> result.get
        is ZLeft -> return ZLeft("Error reading entry $entryIdx: ${result.get}")
      }
    }

    return extractResult(temp).map { DeserializeInfo(it, idx - startIndex) }
  }

  abstract fun init(): Temp
  abstract fun tryRead(data: ByteArray, idx: Int, temp: Temp): ZEither<String, Int>?
  abstract fun extractResult(temp: Temp): ZEither<String, A>

  companion object {
    fun <A> tryRead(
      data: ByteArray, idx: Int, cell: Cell<A?>, rw: IDeserializer<A>, tag: Byte
    ): ZEither<String, Int>? {
      val readTag = data[idx]
      if (readTag != tag) return null

      return when (val currentValue = cell.value) {
        null -> when (val result = rw.deserialize(data, idx + 1)) {
          is ZLeft -> ZLeft("Deserialization of tag $readTag at ${idx + 1} failed with ${result.get}")
          is ZRight -> {
            cell.value = result.get.value
            return ZRight(1 + result.get.bytesRead)
          }
        }
        else -> ZLeft("Field with tag $tag found again, but it already has a value of $currentValue")
      }
    }

    fun <A> Cell<A?>.extract(tag: Byte, deserializer: IDeserializerForTagged<A>): ZEither<String, A> =
      when (val v = value) {
        null -> when (val default = deserializer.defaultValue) {
          null -> ZLeft("Could not read field with tag $tag - no value")
          else -> ZRight(default)
        }
        else -> ZRight(v)
      }
  }

  data class Cells2<A1, A2>(val a1: Cell<A1?> = Cell(null), val a2: Cell<A2?> = Cell(null))
  data class Cells3<A1, A2, A3>(
    val a1: Cell<A1?> = Cell(null), val a2: Cell<A2?> = Cell(null),
    val a3: Cell<A3?> = Cell(null)
  )

  data class Cells4<A1, A2, A3, A4>(
    val a1: Cell<A1?> = Cell(null), val a2: Cell<A2?> = Cell(null),
    val a3: Cell<A3?> = Cell(null), val a4: Cell<A4?> = Cell(null)
  )

  data class Cells5<A1, A2, A3, A4, A5>(
    val a1: Cell<A1?> = Cell(null), val a2: Cell<A2?> = Cell(null),
    val a3: Cell<A3?> = Cell(null), val a4: Cell<A4?> = Cell(null),
    val a5: Cell<A5?> = Cell(null)
  )

  data class Cells6<A1, A2, A3, A4, A5, A6>(
    val a1: Cell<A1?> = Cell(null), val a2: Cell<A2?> = Cell(null),
    val a3: Cell<A3?> = Cell(null), val a4: Cell<A4?> = Cell(null),
    val a5: Cell<A5?> = Cell(null), val a6: Cell<A6?> = Cell(null)
  )
}

class TaggedRW1<A1, R>(
  val tag1: Byte, val a1Rw: ISerializedRWForTagged<A1>,
  val apply: (A1) -> R,
  val e1: (R) -> A1
) : TaggedRW<R, Cell<A1?>>(), ISerializedRW<R> {
  override fun serializeToCollection(a: R): Collection<Pair<Byte, ByteRope>> = listOf(tag1 to a1Rw.serialize(e1(a)))

  override fun init() = Cell<A1?>(null)

  override fun tryRead(data: ByteArray, idx: Int, temp: Cell<A1?>): ZEither<String, Int>? =
    tryRead(data, idx, temp, a1Rw, tag1)

  override fun extractResult(temp: Cell<A1?>): ZEither<String, R> =
    temp.extract(tag1, a1Rw).map { apply(it) }
}

class TaggedRW2<A1, A2, R>(
  val tag1: Byte, val a1Rw: ISerializedRWForTagged<A1>,
  val tag2: Byte, val a2Rw: ISerializedRWForTagged<A2>,
  val apply: (A1, A2) -> R,
  val e1: (R) -> A1,
  val e2: (R) -> A2
) : TaggedRW<R, TaggedRW.Cells2<A1, A2>>(), ISerializedRW<R> {
  override fun serializeToCollection(a: R): Collection<Pair<Byte, ByteRope>> = listOf(
    tag1 to a1Rw.serialize(e1(a)),
    tag2 to a2Rw.serialize(e2(a))
  )

  override fun init() = Cells2(Cell<A1?>(null), Cell<A2?>(null))

  override fun tryRead(data: ByteArray, idx: Int, temp: Cells2<A1, A2>): ZEither<String, Int>? =
    tryRead(data, idx, temp.a1, a1Rw, tag1) ?: tryRead(data, idx, temp.a2, a2Rw, tag2)

  override fun extractResult(temp: Cells2<A1, A2>): ZEither<String, R> =
    temp.a1.extract(tag1, a1Rw).flatMap { a1 ->
      temp.a2.extract(tag2, a2Rw).map { a2 ->
        apply(a1, a2)
      }
    }
}

class TaggedRW3<A1, A2, A3, R>(
  val tag1: Byte, val a1Rw: ISerializedRWForTagged<A1>,
  val tag2: Byte, val a2Rw: ISerializedRWForTagged<A2>,
  val tag3: Byte, val a3Rw: ISerializedRWForTagged<A3>,
  val apply: (A1, A2, A3) -> R,
  val e1: (R) -> A1,
  val e2: (R) -> A2,
  val e3: (R) -> A3
) : TaggedRW<R, TaggedRW.Cells3<A1, A2, A3>>(), ISerializedRW<R> {
  override fun serializeToCollection(a: R): Collection<Pair<Byte, ByteRope>> = listOf(
    tag1 to a1Rw.serialize(e1(a)),
    tag2 to a2Rw.serialize(e2(a)),
    tag3 to a3Rw.serialize(e3(a))
  )

  override fun init() = Cells3<A1, A2, A3>()

  override fun tryRead(data: ByteArray, idx: Int, temp: Cells3<A1, A2, A3>): ZEither<String, Int>? =
    tryRead(data, idx, temp.a1, a1Rw, tag1)
      ?: tryRead(data, idx, temp.a2, a2Rw, tag2)
      ?: tryRead(data, idx, temp.a3, a3Rw, tag3)

  override fun extractResult(temp: Cells3<A1, A2, A3>): ZEither<String, R> =
    temp.a1.extract(tag1, a1Rw).flatMap { a1 ->
      temp.a2.extract(tag2, a2Rw).flatMap { a2 ->
        temp.a3.extract(tag3, a3Rw).map { a3 ->
          apply(a1, a2, a3)
        }
      }
    }
}

class TaggedRW4<A1, A2, A3, A4, R>(
  val tag1: Byte, val a1Rw: ISerializedRWForTagged<A1>,
  val tag2: Byte, val a2Rw: ISerializedRWForTagged<A2>,
  val tag3: Byte, val a3Rw: ISerializedRWForTagged<A3>,
  val tag4: Byte, val a4Rw: ISerializedRWForTagged<A4>,
  val apply: (A1, A2, A3, A4) -> R,
  val e1: (R) -> A1,
  val e2: (R) -> A2,
  val e3: (R) -> A3,
  val e4: (R) -> A4
) : TaggedRW<R, TaggedRW.Cells4<A1, A2, A3, A4>>(), ISerializedRW<R> {
  override fun serializeToCollection(a: R): Collection<Pair<Byte, ByteRope>> = listOf(
    tag1 to a1Rw.serialize(e1(a)),
    tag2 to a2Rw.serialize(e2(a)),
    tag3 to a3Rw.serialize(e3(a)),
    tag4 to a4Rw.serialize(e4(a))
  )

  override fun init() = Cells4<A1, A2, A3, A4>()

  override fun tryRead(data: ByteArray, idx: Int, temp: Cells4<A1, A2, A3, A4>): ZEither<String, Int>? =
    tryRead(data, idx, temp.a1, a1Rw, tag1)
      ?: tryRead(data, idx, temp.a2, a2Rw, tag2)
      ?: tryRead(data, idx, temp.a3, a3Rw, tag3)
      ?: tryRead(data, idx, temp.a4, a4Rw, tag4)

  override fun extractResult(temp: Cells4<A1, A2, A3, A4>): ZEither<String, R> =
    temp.a1.extract(tag1, a1Rw).flatMap { a1 ->
      temp.a2.extract(tag2, a2Rw).flatMap { a2 ->
        temp.a3.extract(tag3, a3Rw).flatMap { a3 ->
          temp.a4.extract(tag4, a4Rw).map { a4 ->
            apply(a1, a2, a3, a4)
          }
        }
      }
    }
}

class TaggedRW5<A1, A2, A3, A4, A5, R>(
  val tag1: Byte, val a1Rw: ISerializedRWForTagged<A1>,
  val tag2: Byte, val a2Rw: ISerializedRWForTagged<A2>,
  val tag3: Byte, val a3Rw: ISerializedRWForTagged<A3>,
  val tag4: Byte, val a4Rw: ISerializedRWForTagged<A4>,
  val tag5: Byte, val a5Rw: ISerializedRWForTagged<A5>,
  val apply: (A1, A2, A3, A4, A5) -> R,
  val e1: (R) -> A1,
  val e2: (R) -> A2,
  val e3: (R) -> A3,
  val e4: (R) -> A4,
  val e5: (R) -> A5
) : TaggedRW<R, TaggedRW.Cells5<A1, A2, A3, A4, A5>>(), ISerializedRW<R> {
  override fun serializeToCollection(a: R): Collection<Pair<Byte, ByteRope>> = listOf(
    tag1 to a1Rw.serialize(e1(a)),
    tag2 to a2Rw.serialize(e2(a)),
    tag3 to a3Rw.serialize(e3(a)),
    tag4 to a4Rw.serialize(e4(a)),
    tag5 to a5Rw.serialize(e5(a))
  )

  override fun init() = Cells5<A1, A2, A3, A4, A5>()

  override fun tryRead(data: ByteArray, idx: Int, temp: Cells5<A1, A2, A3, A4, A5>): ZEither<String, Int>? =
    tryRead(data, idx, temp.a1, a1Rw, tag1)
      ?: tryRead(data, idx, temp.a2, a2Rw, tag2)
      ?: tryRead(data, idx, temp.a3, a3Rw, tag3)
      ?: tryRead(data, idx, temp.a4, a4Rw, tag4)
      ?: tryRead(data, idx, temp.a5, a5Rw, tag5)

  override fun extractResult(temp: Cells5<A1, A2, A3, A4, A5>): ZEither<String, R> =
    temp.a1.extract(tag1, a1Rw).flatMap { a1 ->
      temp.a2.extract(tag2, a2Rw).flatMap { a2 ->
        temp.a3.extract(tag3, a3Rw).flatMap { a3 ->
          temp.a4.extract(tag4, a4Rw).flatMap { a4 ->
            temp.a5.extract(tag5, a5Rw).map { a5 ->
              apply(a1, a2, a3, a4, a5)
            }
          }
        }
      }
    }
}

class TaggedRW6<A1, A2, A3, A4, A5, A6, R>(
  val tag1: Byte, val a1Rw: ISerializedRWForTagged<A1>,
  val tag2: Byte, val a2Rw: ISerializedRWForTagged<A2>,
  val tag3: Byte, val a3Rw: ISerializedRWForTagged<A3>,
  val tag4: Byte, val a4Rw: ISerializedRWForTagged<A4>,
  val tag5: Byte, val a5Rw: ISerializedRWForTagged<A5>,
  val tag6: Byte, val a6Rw: ISerializedRWForTagged<A6>,
  val apply: (A1, A2, A3, A4, A5, A6) -> R,
  val e1: (R) -> A1,
  val e2: (R) -> A2,
  val e3: (R) -> A3,
  val e4: (R) -> A4,
  val e5: (R) -> A5,
  val e6: (R) -> A6
) : TaggedRW<R, TaggedRW.Cells6<A1, A2, A3, A4, A5, A6>>(), ISerializedRW<R> {
  override fun serializeToCollection(a: R): Collection<Pair<Byte, ByteRope>> = listOf(
    tag1 to a1Rw.serialize(e1(a)),
    tag2 to a2Rw.serialize(e2(a)),
    tag3 to a3Rw.serialize(e3(a)),
    tag4 to a4Rw.serialize(e4(a)),
    tag5 to a5Rw.serialize(e5(a)),
    tag6 to a6Rw.serialize(e6(a))
  )

  override fun init() = Cells6<A1, A2, A3, A4, A5, A6>()

  override fun tryRead(data: ByteArray, idx: Int, temp: Cells6<A1, A2, A3, A4, A5, A6>): ZEither<String, Int>? =
    tryRead(data, idx, temp.a1, a1Rw, tag1)
      ?: tryRead(data, idx, temp.a2, a2Rw, tag2)
      ?: tryRead(data, idx, temp.a3, a3Rw, tag3)
      ?: tryRead(data, idx, temp.a4, a4Rw, tag4)
      ?: tryRead(data, idx, temp.a5, a5Rw, tag5)
      ?: tryRead(data, idx, temp.a6, a6Rw, tag6)

  override fun extractResult(temp: Cells6<A1, A2, A3, A4, A5, A6>): ZEither<String, R> =
    temp.a1.extract(tag1, a1Rw).flatMap { a1 ->
      temp.a2.extract(tag2, a2Rw).flatMap { a2 ->
        temp.a3.extract(tag3, a3Rw).flatMap { a3 ->
          temp.a4.extract(tag4, a4Rw).flatMap { a4 ->
            temp.a5.extract(tag5, a5Rw).flatMap { a5 ->
              temp.a6.extract(tag6, a6Rw).map { a6 ->
                apply(a1, a2, a3, a4, a5, a6)
              }
            }
          }
        }
      }
    }
}