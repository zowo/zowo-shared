package app.zowo.shared.lib.serialization

import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.flatMap

class AndDeserializer2<A1, A2, R>(
  val a1Deserializer: IDeserializer<A1>, val a2Deserializer: IDeserializer<A2>,
  val apply: (A1, A2) -> R
) : IDeserializer<R> {
  override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<R>> =
    a1Deserializer.deserialize(serialized, startIndex).flatMap { a1Di ->
      a2Deserializer.deserialize(serialized, startIndex + a1Di.bytesRead).map { a2Di ->
        DeserializeInfo(apply(a1Di.value, a2Di.value), a1Di.bytesRead + a2Di.bytesRead)
      }
    }
}

class AndSerializer2<A1, A2, R>(
  val a1Serializer: ISerializer<A1>, val a2Serializer: ISerializer<A2>,
  val e1: (R) -> A1,
  val e2: (R) -> A2
) : ISerializer<R> {
  override fun serialize(a: R): ByteRope = a1Serializer.serialize(e1(a)) + a2Serializer.serialize(e2(a))
}

class AndRw2<A1, A2, R>(
  a1Rw: ISerializedRW<A1>,
  a2Rw: ISerializedRW<A2>,
  apply: (A1, A2) -> R,
  e1: (R) -> A1,
  e2: (R) -> A2
) : CombinedRW<R>(AndSerializer2(a1Rw, a2Rw, e1, e2), AndDeserializer2(a1Rw, a2Rw, apply))