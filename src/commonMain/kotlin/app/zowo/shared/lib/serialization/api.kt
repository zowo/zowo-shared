package app.zowo.shared.lib.serialization

import kotlinx.collections.immutable.persistentHashMapOf
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.*

interface ISerializable {
  fun serialize(): ByteRope
}

interface ISerializer<in A> {
  fun serialize(a: A): ByteRope

  fun <B> contramap(f: (B) -> A): ISerializer<B> {
    val self = this
    return object : ISerializer<B> {
      override fun serialize(a: B): ByteRope = self.serialize(f(a))
    }
  }
}

object Serializer {
  fun <A, B> pair(aSerializer: ISerializer<A>, bSerializer: ISerializer<B>): ISerializer<Pair<A, B>> =
    AndSerializer2(aSerializer, bSerializer, { it.first }, { it.second })

  fun <A, B> mapEntry(aSerializer: ISerializer<A>, bSerializer: ISerializer<B>): ISerializer<Map.Entry<A, B>> =
    AndSerializer2(aSerializer, bSerializer, { it.key }, { it.value })
}

fun ByteRope.tagWithLength(tag: Byte): ByteRope = ByteRope.Single(SerializedRW.int.serialize(this.size), tag) + this

data class DeserializeInfo<out A>(val value: A, val bytesRead: Int) {
  fun <B> map(f: (A) -> B): DeserializeInfo<B> = DeserializeInfo(f(value), bytesRead)
  fun addBytes(i: Int): DeserializeInfo<A> = copy(bytesRead = bytesRead + i)
}

interface IDeserializer<out A> {
  fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>>

  fun <B> map(f: (A) -> B): IDeserializer<B> {
    val self = this
    return object : IDeserializer<B> {
      override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<B>> =
        self.deserialize(serialized, startIndex).map { it.map(f) }
    }
  }
}

object Deserializer {
  fun <A> const(a: A): IDeserializer<A> = object : IDeserializer<A> {
    val result = ZRight(DeserializeInfo(a, 0))

    override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> = result
  }

  fun <A, B> pair(aDeserializer: IDeserializer<A>, bDeserializer: IDeserializer<B>): IDeserializer<Pair<A, B>> =
    AndDeserializer2(aDeserializer, bDeserializer) { a, b -> Pair(a, b) }

  fun <A, B> mapEntry(aDeserializer: IDeserializer<A>, bDeserializer: IDeserializer<B>): IDeserializer<Map.Entry<A, B>> =
    AndDeserializer2(aDeserializer, bDeserializer) { a, b ->
      object : Map.Entry<A, B> {
        override val key: A
          get() = a
        override val value: B
          get() = b
      }
    }
}

interface ISerializedRW<A> : ISerializer<A>, IDeserializer<A>

fun <A, B> ISerializedRW<A>.inmap(deserialize: (A) -> B, serialize: (B) -> A): ISerializedRW<B> {
  val self = this
  return object : ISerializedRW<B> {
    override fun serialize(a: B): ByteRope = self.serialize(serialize(a))

    override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<B>> =
      self.deserialize(serialized, startIndex).map { it.map(deserialize) }
  }
}

fun <A, B> ISerializedRW<A>.inmapE(
  deserialize: (A) -> ZEither<String, B>, serialize: (B) -> A
): ISerializedRW<B> {
  val self = this
  return object : ISerializedRW<B> {
    override fun serialize(a: B): ByteRope = self.serialize(serialize(a))

    override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<B>> =
      self.deserialize(serialized, startIndex).flatMap { di ->
        deserialize(di.value).map { DeserializeInfo(it, di.bytesRead) }
      }
  }
}

interface HasRw<A> : ISerializable {
  val selfRw: ISerializedRW<A>
}

object OptionRW {
  val NoneByte = 'n'.toByte()
  val SomeByte = 's'.toByte()
}

fun <A> ISerializedRW<A>.opt(): ISerializedRW<ZOption<A>> = DiscriminatedRW(
  {
    when (it) {
      is ZNone -> ByteRope.create(OptionRW.NoneByte)
      is ZSome -> ByteRope.create(OptionRW.SomeByte) + this.serialize(it.get)
    }
  },
  persistentHashMapOf(
    OptionRW.NoneByte to Deserializer.const(ZNone),
    OptionRW.SomeByte to (this.map<ZOption<A>> { ZSome(it) })
  )
)

open class CombinedRW<A>(val serializer: ISerializer<A>, val deserializer: IDeserializer<A>) : ISerializedRW<A> {
  override fun serialize(a: A): ByteRope = serializer.serialize(a)
  override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> =
    deserializer.deserialize(serialized, startIndex)
}

abstract class SerializedRWFuncs<A>(
  private val serializeFunc: (A) -> ByteRope,
  private val deserializeFunc: (ByteArray, Int) -> ZEither<String, DeserializeInfo<A>>
) : ISerializedRW<A> {
  override fun serialize(a: A): ByteRope = serializeFunc(a)
  override fun deserialize(serialized: ByteArray, startIndex: Int): ZEither<String, DeserializeInfo<A>> =
    deserializeFunc(serialized, startIndex)
}