package app.zowo.shared.lib.data

// Mutable heap allocated reference.
class Cell<A>(var value: A) {
  override fun toString(): String = "Cell($value)"
}