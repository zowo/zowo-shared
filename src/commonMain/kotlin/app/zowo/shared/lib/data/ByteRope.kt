package app.zowo.shared.lib.data

sealed class ByteRope(val size: Int) {
  val isEmpty: Boolean = size == 0
  operator fun plus(rope: ByteRope): ByteRope = Cons(this, rope)

  fun toArray(): ByteArray {
    val array = ByteArray(size)
    writeTo(array, 0)
    return array
  }

  private fun writeTo(array: ByteArray, written: Int) {
    when (this) {
      Empty -> {
      }
      is Single -> {
        array[array.size - written - 1] = byte
        previous.writeTo(array, written + 1)
      }
      is Bytes2 -> {
        array[array.size - written - 2] = b1
        array[array.size - written - 1] = b2
        previous.writeTo(array, written + 2)
      }
      is Bytes3 -> {
        array[array.size - written - 3] = b1
        array[array.size - written - 2] = b2
        array[array.size - written - 1] = b3
        previous.writeTo(array, written + 3)
      }
      is Bytes4 -> {
        array[array.size - written - 4] = b1
        array[array.size - written - 3] = b2
        array[array.size - written - 2] = b3
        array[array.size - written - 1] = b4
        previous.writeTo(array, written + 4)
      }
      is Bytes8 -> {
        array[array.size - written - 8] = b1
        array[array.size - written - 7] = b2
        array[array.size - written - 6] = b3
        array[array.size - written - 5] = b4
        array[array.size - written - 4] = b5
        array[array.size - written - 3] = b6
        array[array.size - written - 2] = b7
        array[array.size - written - 1] = b8
        previous.writeTo(array, written + 8)
      }
      is Arr -> {
        current.copyInto(array, array.size - written - current.size)
        previous.writeTo(array, written + current.size)
      }
      is Cons -> {
        current.writeTo(array, written)
        previous.writeTo(array, written + current.size)
      }
    }
  }

  companion object {
    fun create(byte: Byte): ByteRope = Single(Empty, byte)
    fun create(bytes: ByteArray): ByteRope = Arr(Empty, bytes)
    fun create(b1: Byte, b2: Byte): ByteRope = Bytes2(Empty, b1, b2)
    fun create(b1: Byte, b2: Byte, b3: Byte): ByteRope = Bytes3(Empty, b1, b2, b3)
    fun create(b1: Byte, b2: Byte, b3: Byte, b4: Byte): ByteRope = Bytes4(Empty, b1, b2, b3, b4)
    fun create(b1: Byte, b2: Byte, b3: Byte, b4: Byte, b5: Byte, b6: Byte, b7: Byte, b8: Byte): ByteRope =
      Bytes8(Empty, b1, b2, b3, b4, b5, b6, b7, b8)
  }

  object Empty : ByteRope(0)
  class Single(val previous: ByteRope, val byte: Byte) : ByteRope(previous.size + 1)
  class Bytes2(val previous: ByteRope, val b1: Byte, val b2: Byte) : ByteRope(previous.size + 2)
  class Bytes3(val previous: ByteRope, val b1: Byte, val b2: Byte, val b3: Byte) : ByteRope(previous.size + 3)
  class Bytes4(val previous: ByteRope, val b1: Byte, val b2: Byte, val b3: Byte, val b4: Byte) : ByteRope(previous.size + 4)
  class Bytes8(
    val previous: ByteRope,
    val b1: Byte, val b2: Byte, val b3: Byte, val b4: Byte,
    val b5: Byte, val b6: Byte, val b7: Byte, val b8: Byte
  ) : ByteRope(previous.size + 8)

  class Arr(val previous: ByteRope, val current: ByteArray) : ByteRope(previous.size + current.size)
  class Cons(val previous: ByteRope, val current: ByteRope) : ByteRope(previous.size + current.size)
}