package app.zowo.shared.lib.exts

import kotlinx.collections.immutable.*

fun <A, B> PersistentList<A>.map(f: (A) -> B): PersistentList<B> =
  persistentListOf<B>().mutate {
    for (a in this) it.add(f(a))
  }

fun <K, V, V1> PersistentMap<K, V>.mapValues(f: (K, V) -> V1): PersistentMap<K, V1> {
  val b = persistentHashMapOf<K, V1>().builder()
  for (entry in this) b[entry.key] = f(entry.key, entry.value)
  return b.build()
}