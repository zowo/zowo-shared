package app.zowo.shared.lib.exts

fun <A, B> Comparator<A>.contramap(f: (B) -> A): Comparator<B> {
  val self = this
  return object : Comparator<B> {
    override fun compare(a: B, b: B): Int = self.compare(f(a), f(b))
  }
}