package app.zowo.shared.lib.exts

import kotlinx.collections.immutable.*
import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZLeft
import app.zowo.shared.lib.functional.ZRight

fun <Element, L, R> Iterable<Element>.foldLeftEither(initial: R, f: (R, Element) -> ZEither<L, R>): ZEither<L, R> {
  var current = initial
  for (element in this) {
    when (val result = f(current, element)) {
      is ZLeft -> return ZLeft(result.get)
      is ZRight -> current = result.get
    }
  }
  return ZRight(current)
}

fun <A> Iterable<A>.indexWhere(predicate: (A) -> Boolean): Pair<Int, A>? {
  var idx = 0
  for (element in this) {
    if (predicate(element)) return Pair(idx, element)
    idx++
  }
  return null
}

data class FoldEitherResult<Element, L, R>(val result: R, val errors: PersistentList<Pair<Element, L>>)

fun <Element, L, R> Iterable<Element>.foldLeftEitherErrors(
  initial: R, f: (R, Element) -> ZEither<L, R>
): FoldEitherResult<Element, L, R> {
  val errorBuilder = persistentListOf<Pair<Element, L>>().builder()
  var current = initial
  for (element in this) {
    when (val result = f(current, element)) {
      is ZLeft -> errorBuilder.add(Pair(element, result.get))
      is ZRight -> current = result.get
    }
  }
  return FoldEitherResult(current, errorBuilder.build())
}

inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
  var sum: Long = 0
  for (element in this) {
    sum += selector(element)
  }
  return sum
}

fun <K, V> Iterable<Pair<K, V>>.toPersistentHashMap(): PersistentMap<K, V> =
  persistentHashMapOf<K, V>().mutate { for (kv in this) it[kv.first] = kv.second }