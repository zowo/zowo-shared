package app.zowo.shared.lib.exts

@Suppress("NOTHING_TO_INLINE", "ConvertTwoComparisonsToRangeCheck")
inline fun ByteArray.validIndex(idx: Int): Boolean = idx >= 0 && idx < size

@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.validIndexes(idxStart: Int, idxEndInclusive: Int): Boolean =
  idxEndInclusive >= idxStart && validIndex(idxStart) && validIndex(idxEndInclusive)