package app.zowo.shared.lib.functional

sealed class ZEither<out A, out B> {
  companion object {
    fun <B> fromTry(f: () -> B): ZEither<Throwable, B> =
      try {
        ZRight(f())
      } catch (t: Throwable) {
        ZLeft(t)
      }
  }

  abstract val unsafeGetLeft: A
  abstract val unsafeGetRight: B
  abstract val isLeft: Boolean
  abstract val isRight: Boolean

  abstract fun <B1> map(f: (B) -> B1): ZEither<A, B1>
  abstract fun <A1> mapLeft(f: (A) -> A1): ZEither<A1, B>

  fun <C> fold(ifLeft: (A) -> C, ifRight: (B) -> C): C = when (this) {
    is ZLeft -> ifLeft(get)
    is ZRight -> ifRight(get)
  }

  fun swap(): ZEither<B, A> = when (this) {
    is ZLeft -> ZRight(get)
    is ZRight -> ZLeft(get)
  }

  val getOrThrow: B
    get() = when (this) {
      is ZLeft -> throw Exception("Expected ZRight, but was $this")
      is ZRight -> get
    }
}

fun <A, A1, B, B1> ZEither<A, B>.flatMap(f: (B) -> ZEither<A1, B1>): ZEither<A1, B1> where A : A1 = when (this) {
  is ZLeft -> this
  is ZRight -> f(get)
}

fun <A, B, B1> ZEither<A, B>.getOrElse(orElse: B1) where B : B1 = when (this) {
  is ZLeft -> orElse
  is ZRight -> get
}

fun <A, B, B1> ZEither<A, B>.getOrElse(orElse: () -> B1) where B : B1 = when (this) {
  is ZLeft -> orElse()
  is ZRight -> get
}

fun <Left, Right> Left?.toLeft(ifNull: Right): ZEither<Left, Right> =
  if (this == null) ZRight(ifNull) else ZLeft(this)

fun <Left, Right> Right?.toRight(ifNull: Left): ZEither<Left, Right> =
  if (this == null) ZLeft(ifNull) else ZRight(this)

data class ZLeft<out A>(val get: A) : ZEither<A, Nothing>() {
  override val isLeft: Boolean
    get() = true
  override val isRight: Boolean
    get() = false
  override val unsafeGetLeft: A
    get() = get
  override val unsafeGetRight: Nothing
    get() = throw Exception("unsafeGetRight on ZLeft($unsafeGetLeft)")

  override fun <B1> map(f: (Nothing) -> B1): ZEither<A, B1> = this
  override fun <A1> mapLeft(f: (A) -> A1): ZEither<A1, Nothing> = ZLeft(f(get))
  override fun toString(): String = "ZLeft($get)"
}

data class ZRight<out B>(val get: B) : ZEither<Nothing, B>() {
  override val isLeft: Boolean
    get() = false
  override val isRight: Boolean
    get() = true
  override val unsafeGetLeft: Nothing
    get() = throw Exception("unsafeGetLeft on ZRight($unsafeGetRight)")
  override val unsafeGetRight: B
    get() = get

  override fun <B1> map(f: (B) -> B1): ZEither<Nothing, B1> = ZRight(f(get))
  override fun <A1> mapLeft(f: (Nothing) -> A1): ZEither<A1, B> = this
  override fun toString(): String = "ZRight($get)"
}