package app.zowo.shared.lib.functional

import kotlinx.collections.immutable.*
import app.zowo.shared.lib.exts.map
import app.zowo.shared.lib.serialization.ISerializedRW
import app.zowo.shared.lib.serialization.inmapE
import kotlin.jvm.JvmStatic

// Marker class for non-emptyness.
data class ZNonEmpty<A>(val value: A) {
  companion object {
    @Suppress("unused")
    @JvmStatic
    fun <A> rw(aRw: ISerializedRW<A>, isEmpty: (A) -> Boolean): ISerializedRW<ZNonEmpty<A>> =
      aRw.inmapE(
        { if (isEmpty(it)) ZLeft("Can't create from $it: empty") else ZRight(ZNonEmpty(it)) },
        { it.value }
      )

    @Suppress("unused")
    @JvmStatic
    fun <A : Collection<*>> rw(aRw: ISerializedRW<A>): ISerializedRW<ZNonEmpty<A>> =
      rw(aRw) { it.isEmpty() }

    @Suppress("unused")
    @JvmStatic
    fun <A> list(first: A, vararg rest: A): ZNonEmpty<PersistentList<A>> =
      ZNonEmpty(persistentListOf(first).mutate { for (a in rest) it.add(a) })

    @Suppress("unused")
    @JvmStatic
    fun <A> set(first: A, vararg rest: A): ZNonEmpty<PersistentSet<A>> =
      ZNonEmpty(persistentSetOf(first).mutate { for (a in rest) it.add(a) })

    @Suppress("unused")
    @JvmStatic
    fun <A, C : ImmutableCollection<A>> create(c: C): ZOption<ZNonEmpty<C>> =
      if (c.isEmpty()) ZNone else ZSome(ZNonEmpty(c))
  }
}

fun <A, B> ZNonEmpty<PersistentList<A>>.map(f: (A) -> B) = ZNonEmpty(value.map(f))