package app.zowo.shared.lib.functional

import kotlin.jvm.JvmStatic

sealed class ZOption<out A> {
  companion object {
    @JvmStatic
    fun <A> empty(): ZOption<A> = ZNone

    @JvmStatic
    fun <A> some(a: A): ZOption<A> = ZSome(a)

    @JvmStatic
    fun <A> create(a: A?): ZOption<A> = if (a == null) ZNone else ZSome(a)
  }

  abstract val unsafeGet: A
  abstract val isSome: Boolean
  abstract val isNone: Boolean

  val orNull: A?
    get() = when (this) {
      is ZNone -> null
      is ZSome -> get
    }

  abstract fun <C> fold(ifNone: C, ifSome: (A) -> C): C
  abstract fun <C> fold(ifNone: () -> C, ifSome: (A) -> C): C

  fun exists(f: (A) -> Boolean): Boolean = when (this) {
    ZNone -> false
    is ZSome -> f(get)
  }

  fun <Data> exists(data: Data, f: (A, Data) -> Boolean): Boolean = when (this) {
    ZNone -> false
    is ZSome -> f(get, data)
  }
}

fun <A> ZOption<A>.exists(a: A): Boolean = when (this) {
  ZNone -> false
  is ZSome -> get == a
}

fun <A> A?.asZOption(): ZOption<A> =
  if (this == null) ZNone else ZSome(this)

object ZNone : ZOption<Nothing>() {
  override val isSome: Boolean
    get() = false
  override val isNone: Boolean
    get() = true
  override val unsafeGet: Nothing
    get() = throw Exception("unsafeGet on None!")

  override fun <C> fold(ifNone: C, ifSome: (Nothing) -> C): C = ifNone
  override fun <C> fold(ifNone: () -> C, ifSome: (Nothing) -> C): C = ifNone()

  override fun toString(): String = "ZNone"
}

data class ZSome<A>(val get: A) : ZOption<A>() {
  override val isSome: Boolean
    get() = true
  override val isNone: Boolean
    get() = false
  override val unsafeGet: A
    get() = get

  override fun <C> fold(ifNone: C, ifSome: (A) -> C): C = ifSome(get)
  override fun <C> fold(ifNone: () -> C, ifSome: (A) -> C): C = ifSome(get)

  override fun toString(): String = "ZSome($get)"
}