package app.zowo.shared.lib.compat

import kotlinx.collections.immutable.*
import kotlin.jvm.JvmStatic

object Empty {
  private val nothingList = persistentListOf<Nothing>()
  private val nothingSet = persistentSetOf<Nothing>()
  private val nothingMap = persistentMapOf<Nothing, Nothing>()

  @JvmStatic
  fun <A> list(): PersistentList<A> = nothingList

  @JvmStatic
  fun <A> set(): PersistentSet<A> = nothingSet

  @Suppress("UNCHECKED_CAST")
  @JvmStatic
  fun <K, V> map(): PersistentMap<K, V> = nothingMap as PersistentMap<K, V>
}