package app.zowo.shared.lib.compat

import kotlinx.collections.immutable.ImmutableCollection
import kotlinx.collections.immutable.ImmutableSet
import kotlinx.collections.immutable.PersistentMap
import app.zowo.shared.lib.functional.ZOption

interface ICompatPersistentMap<K, V> {
  val entries: ImmutableSet<Map.Entry<K, V>>
  val keys: ImmutableSet<K>
  val size: Int
  val values: ImmutableCollection<V>
  fun containsKey(key: K): Boolean
  fun containsValue(value: V): Boolean
  fun get(key: K): ZOption<V>
  fun isEmpty(): Boolean
  fun put(key: K, value: V): PersistentMap<K, V>
  fun putAll(m: Map<out K, V>): PersistentMap<K, V>
  fun remove(key: K): PersistentMap<K, V>
  fun remove(key: K, value: V): PersistentMap<K, V>
  fun builder(): PersistentMap.Builder<K, V>
}

@Suppress("unused")
class CompatPersistentMap<K, V>(val backing: ICompatPersistentMap<K, V>) : PersistentMap<K, V> {
  override val entries: ImmutableSet<Map.Entry<K, V>>
    get() = backing.entries
  override val keys: ImmutableSet<K>
    get() = backing.keys
  override val size: Int
    get() = backing.size
  override val values: ImmutableCollection<V>
    get() = backing.values

  override fun builder(): PersistentMap.Builder<K, V> = backing.builder()
  override fun clear(): PersistentMap<K, V> = Empty.map()
  override fun containsKey(key: K): Boolean = backing.containsKey(key)
  override fun containsValue(value: V): Boolean = backing.containsValue(value)
  override fun get(key: K): V? = backing.get(key).orNull
  override fun isEmpty(): Boolean = backing.isEmpty()
  override fun put(key: K, value: V): PersistentMap<K, V> = backing.put(key, value)
  override fun putAll(m: Map<out K, V>): PersistentMap<K, V> = backing.putAll(m)
  override fun remove(key: K): PersistentMap<K, V> = backing.remove(key)
  override fun remove(key: K, value: V): PersistentMap<K, V> = backing.remove(key, value)
}