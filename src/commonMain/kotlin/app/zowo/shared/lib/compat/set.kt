package app.zowo.shared.lib.compat

import kotlinx.collections.immutable.PersistentSet

interface ICompatPersistentSet<A> {
  val size: Int
  fun add(element: A): PersistentSet<A>
  fun addAll(elements: Collection<A>): PersistentSet<A>
  fun builder(): PersistentSet.Builder<A>
  fun contains(element: A): Boolean
  fun containsAll(elements: Collection<A>): Boolean
  fun isEmpty(): Boolean
  fun iterator(): Iterator<A>
  fun remove(element: A): PersistentSet<A>
  fun removeAll(predicate: (A) -> Boolean): PersistentSet<A>
  fun removeAll(elements: Collection<A>): PersistentSet<A>
}

@Suppress("unused")
class CompatPersistentSet<A>(val backing: ICompatPersistentSet<A>) : PersistentSet<A> {
  override val size: Int
    get() = backing.size

  override fun add(element: A): PersistentSet<A> = backing.add(element)
  override fun addAll(elements: Collection<A>): PersistentSet<A> = backing.addAll(elements)
  override fun builder(): PersistentSet.Builder<A> = backing.builder()
  override fun clear(): PersistentSet<A> = Empty.set()
  override fun contains(element: A): Boolean = backing.contains(element)
  override fun containsAll(elements: Collection<A>): Boolean = backing.containsAll(elements)
  override fun isEmpty(): Boolean = backing.isEmpty()
  override fun iterator(): Iterator<A> = backing.iterator()
  override fun remove(element: A): PersistentSet<A> = backing.remove(element)
  override fun removeAll(predicate: (A) -> Boolean): PersistentSet<A> = backing.removeAll(predicate)
  override fun removeAll(elements: Collection<A>): PersistentSet<A> = backing.removeAll(elements)
}