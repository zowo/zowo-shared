package app.zowo.shared.lib.compat

import kotlinx.collections.immutable.PersistentList

interface ICompatPersistentList<A> {
  val size: Int
  fun add(element: A): PersistentList<A>
  fun add(index: Int, element: A): PersistentList<A>
  fun addAll(index: Int, c: Collection<A>): PersistentList<A>
  fun addAll(elements: Collection<A>): PersistentList<A>
  fun builder(): PersistentList.Builder<A>
  fun contains(element: A): Boolean
  fun containsAll(elements: Collection<A>): Boolean
  fun get(index: Int): A
  fun indexOf(element: A): Int
  fun isEmpty(): Boolean
  fun iterator(): Iterator<A>
  fun lastIndexOf(element: A): Int
  fun listIterator(): ListIterator<A>
  fun listIterator(index: Int): ListIterator<A>
  fun remove(element: A): PersistentList<A>
  fun removeAll(predicate: (A) -> Boolean): PersistentList<A>
  fun removeAll(elements: Collection<A>): PersistentList<A>
  fun removeAt(index: Int): PersistentList<A>
  fun set(index: Int, element: A): PersistentList<A>
}

@Suppress("unused")
class CompatPersistentList<A>(val backing: ICompatPersistentList<A>) : PersistentList<A> {
  override val size: Int
    get() = backing.size

  override fun add(element: A): PersistentList<A> = backing.add(element)
  override fun add(index: Int, element: A): PersistentList<A> = backing.add(index, element)
  override fun addAll(index: Int, c: Collection<A>): PersistentList<A> = backing.addAll(index, c)
  override fun addAll(elements: Collection<A>): PersistentList<A> = backing.addAll(elements)
  override fun builder(): PersistentList.Builder<A> = backing.builder()
  override fun clear(): PersistentList<A> = Empty.list()
  override fun contains(element: A): Boolean = backing.contains(element)
  override fun containsAll(elements: Collection<A>): Boolean = backing.containsAll(elements)
  override fun get(index: Int): A = backing.get(index)
  override fun indexOf(element: A): Int = backing.indexOf(element)
  override fun isEmpty(): Boolean = backing.isEmpty()
  override fun iterator(): Iterator<A> = backing.iterator()
  override fun lastIndexOf(element: A): Int = backing.lastIndexOf(element)
  override fun listIterator(): ListIterator<A> = backing.listIterator()
  override fun listIterator(index: Int): ListIterator<A> = backing.listIterator(index)
  override fun remove(element: A): PersistentList<A> = backing.remove(element)
  override fun removeAll(predicate: (A) -> Boolean): PersistentList<A> = backing.removeAll(predicate)
  override fun removeAll(elements: Collection<A>): PersistentList<A> = backing.removeAll(elements)
  override fun removeAt(index: Int): PersistentList<A> = backing.removeAt(index)
  override fun set(index: Int, element: A): PersistentList<A> = backing.set(index, element)
}