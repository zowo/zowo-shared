package app.zowo.shared.protocol.data

import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.exts.contramap
import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.lib.serialization.*
import kotlinx.collections.immutable.persistentHashMapOf
import kotlinx.collections.immutable.persistentMapOf
import kotlin.jvm.JvmStatic
import kotlin.random.Random

// Use the name GUID instead of UUID to prevent clashing with java.util.UUID on JVM and UUID on swift
data class GUID(val l1: Long, val l2: Long) {
  companion object {
    @JvmStatic
    fun random(): GUID = GUID(Random.nextLong(), Random.nextLong())

    @JvmStatic
    val rw = AndRw2(SerializedRW.long, SerializedRW.long, { l1, l2 -> GUID(l1, l2) }, { it.l1 }, { it.l2 })
  }
}

data class DeviceId(val id: Short) {
  fun next() = DeviceId((id + 1).toShort())

  companion object {
    @JvmStatic
    val rw = SerializedRW.short.inmap({ DeviceId(it) }, { it.id })
    // Min value! Other code depends on this.
    @Suppress("unused")
    @JvmStatic
    val Server = DeviceId(Short.MIN_VALUE)

    @Suppress("unused")
    @JvmStatic
    fun parse(s: String): ZEither<String, DeviceId> =
      ZEither.fromTry { DeviceId(s.toShort()) }
        .mapLeft { "Can't parse '$s' as DeviceId: $it" }
  }
}

data class LocalEventId(val id: Int) : Comparable<LocalEventId> {
  companion object {
    @JvmStatic
    val MIN = LocalEventId(Int.MIN_VALUE)
    @JvmStatic
    val rw = SerializedRW.int.inmap({ LocalEventId(it) }, { it.id })
    @JvmStatic
    val comparator = Comparator<LocalEventId> { t, t2 -> t.id.compareTo(t2.id) }
  }

  override operator fun compareTo(other: LocalEventId): Int = comparator.compare(this, other)

  val next: LocalEventId
    get() = LocalEventId(id + 1)
}

data class FirebaseUserId(val id: String) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = SerializedRW.string.inmap({ FirebaseUserId(it) }, { it.id })
  }
}

data class EventRef(
  val deviceId: DeviceId, val localEventId: LocalEventId
) : Comparable<EventRef> {
  companion object {
    @JvmStatic
    val rw = TaggedRW2(
      0, DeviceId.rw.failIfMissing(),
      1, LocalEventId.rw.failIfMissing(),
      { deviceId, localEventId -> EventRef(deviceId, localEventId) },
      { it.deviceId }, { it.localEventId }
    )
    @JvmStatic
    val comparator = LocalEventId.comparator.contramap { r: EventRef -> r.localEventId }
  }

  override fun compareTo(other: EventRef): Int = comparator.compare(this, other)
}

// Randomly generated user id which should have enough randomness to serve as a unique ID per user.
data class PerUserId(val id: Long) {
  companion object {
    @JvmStatic
    fun random(): PerUserId = PerUserId(Random.nextLong())

    @JvmStatic
    val rw = SerializedRW.long.inmap({ PerUserId(it) }, { it.id })
  }
}

data class AccountId(val id: PerUserId) {
  companion object {
    @Suppress("unused")
    @JvmStatic
    fun random(): AccountId = AccountId(PerUserId.random())

    @JvmStatic
    val rw = PerUserId.rw.inmap({ AccountId(it) }, { it.id })
  }
}

data class IconId(val id: Short) {
  companion object {
    @JvmStatic
    val rw = SerializedRW.short.inmap({ IconId(it) }, { it.id })
  }
}

data class CategoryId(val id: PerUserId) {
  companion object {
    fun of(l: Long): CategoryId = CategoryId(PerUserId(l))

    @Suppress("unused")
    @JvmStatic
    fun random(): CategoryId = CategoryId(PerUserId.random())

    @JvmStatic
    val rw = PerUserId.rw.inmap({ CategoryId(it) }, { it.id })
  }
}

data class MerchantId(val id: PerUserId) {
  companion object {
    @Suppress("unused")
    @JvmStatic
    fun random(): MerchantId = MerchantId(PerUserId.random())

    @JvmStatic
    val rw = PerUserId.rw.inmap({ MerchantId(it) }, { it.id })
  }
}

data class AccountName(val name: String) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = SerializedRW.string.inmap({ AccountName(it) }, { it.name })
  }
}

data class CategoryName(val name: String) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = SerializedRW.string.inmap({ CategoryName(it) }, { it.name })
  }
}

// count of days since day 0 which is is 1970-01-01 (ISO)
data class LocalDate(val days: Long) : Comparable<LocalDate> {
  companion object {
    @JvmStatic
    val rw = SerializedRW.long.inmap({ LocalDate(it) }, { it.days })

    @JvmStatic
    val comparator = Comparator<LocalDate> { a, b -> a.days.compareTo(b.days) }
  }

  override fun compareTo(other: LocalDate) = comparator.compare(this, other)
}

// seconds since start of day (midnight)
data class LocalTime(val seconds: Int) {
  companion object {
    @JvmStatic
    val rw = SerializedRW.int.inmap({ LocalTime(it) }, { it.seconds })
  }
}

data class IBAN(val iban: String) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = SerializedRW.string.inmap({ IBAN(it) }, { it.iban })

    // TODO: implement validation
    @Suppress("unused")
    @JvmStatic
    fun parse(s: String): ZEither<String, IBAN> = ZRight(IBAN(s))
  }
}

data class Currency(val code: String) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = SerializedRW.string.inmap({ Currency(it) }, { it.code })
  }
}

data class Amount(val cents: Long) {
  companion object {
    @JvmStatic
    val rw = SerializedRW.long.inmap({ Amount(it) }, { it.cents })
  }

  operator fun plus(amount: Amount): Amount = Amount(cents + amount.cents)
  operator fun minus(amount: Amount): Amount = Amount(cents - amount.cents)
  operator fun unaryMinus(): Amount = Amount(-cents)
  @Suppress("unused", "FunctionName") // Scala method
  fun `unary_$minus`(): Amount = unaryMinus()
}

data class KevinAccountId(val id: String) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = SerializedRW.string.inmap({ KevinAccountId(it) }, { it.id })
  }
}

data class SharingRulesetId(val id: GUID) {
  companion object {
    @JvmStatic
    val rw = GUID.rw.inmap({ SharingRulesetId(it) }, { it.id })

    @Suppress("unused")
    @JvmStatic
    fun random() = SharingRulesetId(GUID.random())
  }
}

data class ShareId(val id: GUID) {
  companion object {
    @JvmStatic
    val rw = GUID.rw.inmap({ ShareId(it) }, { it.id })

    @Suppress("unused")
    @JvmStatic
    fun random() = ShareId(GUID.random())
  }
}

data class CategorizedAmount(val categoryId: ZOption<CategoryId>, val note: ZOption<String>, val amount: Amount) {
  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = TaggedRW3(
      0, CategoryId.rw.noneIfMissing(),
      1, SerializedRW.string.noneIfMissing(),
      2, Amount.rw.failIfMissing(),
      { categoryId, note, amount -> CategorizedAmount(categoryId, note, amount) },
      { it.categoryId }, { it.note }, { it.amount }
    )
  }
}

data class AccountIdWithAmount(val id: AccountId, val amount: Amount) {
  companion object {
    @JvmStatic
    val rw = TaggedRW2(
      0, AccountId.rw.failIfMissing(),
      1, Amount.rw.failIfMissing(),
      { a1, a2 -> AccountIdWithAmount(a1, a2) },
      { it.id }, { it.amount }
    )
  }
}

sealed class AccountType {
  object NoIdentifier : AccountType()
  data class Bank(val iban: IBAN) : AccountType() {
    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = TaggedRW1(0, IBAN.rw.failIfMissing(), { Bank(it) }, { it.iban })
    }
  }

  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw: ISerializedRW<AccountType> by lazy {
      DiscriminatedRW(
        {
          when (it) {
            NoIdentifier -> ByteRope.create(0)
            is Bank -> ByteRope.create(1) + Bank.rw.serialize(it)
          }
        },
        persistentMapOf(
          0.toByte() to Deserializer.const(NoIdentifier),
          1.toByte() to Bank.rw
        )
      )
    }
  }
}

sealed class AccountMetadata {
  data class Kevin(val id: KevinAccountId) : AccountMetadata() {
    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = TaggedRW1(0, KevinAccountId.rw.failIfMissing(), { Kevin(it) }, { it.id })
    }
  }

  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw: ISerializedRW<AccountMetadata> by lazy {
      DiscriminatedRW(
        {
          when (it) {
            is Kevin -> ByteRope.create(0) + Kevin.rw.serialize(it)
          }
        },
        persistentHashMapOf<Byte, IDeserializer<AccountMetadata>>(0.toByte() to Kevin.rw)
      )
    }
  }
}