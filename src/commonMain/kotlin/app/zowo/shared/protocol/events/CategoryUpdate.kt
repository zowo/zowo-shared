package app.zowo.shared.protocol.events

import kotlinx.collections.immutable.persistentHashMapOf
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.protocol.data.IconId

sealed class CategoryUpdate {
  data class SetName(val name: String) : CategoryUpdate() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW1(0, SerializedRW.string.failIfMissing(), { SetName(it) }, { it.name })
    }
  }

  data class SetIcon(val icon: ZOption<IconId>) : CategoryUpdate() {
    companion object {
      val rw = TaggedRW1(0, IconId.rw.noneIfMissing(), { SetIcon(it) }, { it.icon })
    }
  }

  companion object {
    @ExperimentalStdlibApi
    val rw: ISerializedRW<CategoryUpdate> by lazy {
      DiscriminatedRW(
        {
          when (it) {
            is SetName -> ByteRope.create(0) + SetName.rw.serialize(it)
            is SetIcon -> ByteRope.create(1) + SetIcon.rw.serialize(it)
          }
        },
        persistentHashMapOf(
          0.toByte() to SetName.rw,
          1.toByte() to SetIcon.rw
        )
      )
    }
  }
}