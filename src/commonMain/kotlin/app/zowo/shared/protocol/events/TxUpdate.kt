package app.zowo.shared.protocol.events

import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.persistentMapOf
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZNonEmpty
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.lib.serialization.SerializedRW.persistentListRW
import app.zowo.shared.protocol.data.*

sealed class TxUpdate {
  data class SetAccountType(val v: AccountType) : TxUpdate() {
    companion object {
      @ExperimentalStdlibApi
      val rw = AccountType.rw.inmap({ SetAccountType(it) }, { it.v })
    }
  }

  data class SetDate(val v: LocalDate) : TxUpdate() {
    companion object {
      val rw = LocalDate.rw.inmap({ SetDate(it) }, { it.v })
    }
  }

  data class SetTime(val v: ZOption<LocalTime>) : TxUpdate() {
    companion object {
      val rw = LocalTime.rw.opt().inmap({ SetTime(it) }, { it.v })
    }
  }

  data class SetIsPrivate(val v: Boolean) : TxUpdate() {
    companion object {
      val rw = SerializedRW.bool.inmap({ SetIsPrivate(it) }, { it.v })
    }
  }

  data class SetNote(val v: ZOption<String>) : TxUpdate() {
    companion object {
      @ExperimentalStdlibApi
      val rw = SerializedRW.string.opt().inmap({ SetNote(it) }, { it.v })
    }
  }

  data class SetMerchant(val v: ZOption<MerchantId>) : TxUpdate() {
    companion object {
      val rw = MerchantId.rw.opt().inmap({ SetMerchant(it) }, { it.v })
    }
  }

  data class SetAmounts(val v: ZNonEmpty<PersistentList<CategorizedAmount>>) : TxUpdate() {
    companion object {
      @ExperimentalStdlibApi
      val rw = ZNonEmpty.rw(persistentListRW(CategorizedAmount.rw)).inmap({SetAmounts(it)}, {it.v})
    }
  }

  companion object {
    @ExperimentalStdlibApi
    val rw: ISerializedRW<TxUpdate> by lazy {
      DiscriminatedRW(
        {
          when (it) {
            is SetAccountType -> ByteRope.create(0) + SetAccountType.rw.serialize(it)
            is SetDate        -> ByteRope.create(1) + SetDate.rw.serialize(it)
            is SetTime        -> ByteRope.create(2) + SetTime.rw.serialize(it)
            is SetIsPrivate   -> ByteRope.create(3) + SetIsPrivate.rw.serialize(it)
            is SetNote        -> ByteRope.create(4) + SetNote.rw.serialize(it)
            is SetMerchant    -> ByteRope.create(5) + SetMerchant.rw.serialize(it)
            is SetAmounts     -> ByteRope.create(6) + SetAmounts.rw.serialize(it)
          }
        },
        persistentMapOf(
          0.toByte() to SetAccountType.rw,
          1.toByte() to SetDate.rw,
          2.toByte() to SetTime.rw,
          3.toByte() to SetIsPrivate.rw,
          4.toByte() to SetNote.rw,
          5.toByte() to SetMerchant.rw,
          6.toByte() to SetAmounts.rw
        )
      )
    }
  }
}
