package app.zowo.shared.protocol.events

import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.persistentHashMapOf
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZNonEmpty
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.protocol.data.*
import kotlin.jvm.JvmStatic


sealed class EventPayload : ISerializable {
  @ExperimentalStdlibApi
  override fun serialize(): ByteRope = rw.serialize(this)

  data class CreateAccount(
    val id: AccountId, val name: AccountName, val type: AccountType, val currency: Currency,
    val initial: Amount, val metadata: ZOption<AccountMetadata>
  ) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW6(
        0, AccountId.rw.failIfMissing(),
        1, AccountName.rw.failIfMissing(),
        2, AccountType.rw.failIfMissing(),
        3, Currency.rw.failIfMissing(),
        4, Amount.rw.failIfMissing(),
        5, AccountMetadata.rw.noneIfMissing(),
        { a1, a2, a3, a4, a5, a6 -> CreateAccount(a1, a2, a3, a4, a5, a6) },
        { it.id }, { it.name }, { it.type }, { it.currency }, { it.initial }, { it.metadata }
      )
    }
  }

  data class Income(val data: TxData) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW1(0, TxData.rw.failIfMissing(), { Income(it) }, { it.data })
    }
  }

  data class Expense(val data: TxData) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW1(0, TxData.rw.failIfMissing(), { Expense(it) }, { it.data })
    }
  }

  data class Transfer(
    val from: AccountIdWithAmount, val to: AccountIdWithAmount,
    val timestamp: TxTimestamp, val meta: TxCommonMeta
  ) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW4(
        0, AccountIdWithAmount.rw.failIfMissing(),
        1, AccountIdWithAmount.rw.failIfMissing(),
        2, TxTimestamp.rw.failIfMissing(),
        3, TxCommonMeta.rw.failIfMissing(),
        { a1, a2, a3, a4 -> Transfer(a1, a2, a3, a4) },
        { it.from }, { it.to }, { it.timestamp }, { it.meta }
      )
    }
  }

  data class CreateMerchant(val id: MerchantId, val name: String) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW2(
        0, MerchantId.rw.failIfMissing(),
        1, SerializedRW.string.failIfMissing(),
        { a1, a2 -> CreateMerchant(a1, a2) },
        { it.id }, { it.name }
      )
    }
  }

  data class LinkMerchants(val from: MerchantId, val to: MerchantId) : EventPayload() {
    companion object {
      val rw = TaggedRW2(
        0, MerchantId.rw.failIfMissing(),
        1, MerchantId.rw.failIfMissing(),
        { a1, a2 -> LinkMerchants(a1, a2) }, { it.from }, { it.to }
      )
    }
  }

  data class UpdateTransaction(val ref: EventRef, val updates: ZNonEmpty<PersistentList<TxUpdate>>) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW2(
        0, EventRef.rw.failIfMissing(),
        1, ZNonEmpty.rw(SerializedRW.persistentListRW(TxUpdate.rw)).failIfMissing(),
        { a1, a2 -> UpdateTransaction(a1, a2) }, { it.ref }, { it.updates }
      )
    }
  }

  data class CreateCategory(val id: CategoryId, val name: CategoryName, val icon: ZOption<IconId>) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW3(
        0, CategoryId.rw.failIfMissing(),
        1, CategoryName.rw.failIfMissing(),
        3, IconId.rw.noneIfMissing(),
        { a1, a2, a3 -> CreateCategory(a1, a2, a3) }, { it.id }, { it.name }, { it.icon }
      )
    }
  }

  data class UpdateCategory(
    val id: CategoryId, val updates: ZNonEmpty<PersistentList<CategoryUpdate>>
  ) : EventPayload() {
    companion object {
      @ExperimentalStdlibApi
      val rw = TaggedRW2(
        0, CategoryId.rw.failIfMissing(),
        1, ZNonEmpty.rw(SerializedRW.persistentListRW(CategoryUpdate.rw)).failIfMissing(),
        { a1, a2 -> UpdateCategory(a1, a2) }, { it.id }, { it.updates }
      )
    }
  }

  data class DeleteCategory(
    val id: CategoryId, val transferTo: CategoryId
  ) : EventPayload() {
    companion object {
      val rw = TaggedRW2(
        0, CategoryId.rw.failIfMissing(),
        1, CategoryId.rw.failIfMissing(),
        { a1, a2 -> DeleteCategory(a1, a2) }, { it.id }, { it.transferTo }
      )
    }
  }

  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw by lazy {
      DiscriminatedRW(
        {
          when (it) {
            is CreateAccount     -> ByteRope.create(0) + CreateAccount.rw.serialize(it)
            is Income            -> ByteRope.create(1) + Income.rw.serialize(it)
            is Expense           -> ByteRope.create(2) + Expense.rw.serialize(it)
            is Transfer          -> ByteRope.create(3) + Transfer.rw.serialize(it)
            is CreateMerchant    -> ByteRope.create(4) + CreateMerchant.rw.serialize(it)
            is LinkMerchants     -> ByteRope.create(5) + LinkMerchants.rw.serialize(it)
            is UpdateTransaction -> ByteRope.create(6) + UpdateTransaction.rw.serialize(it)
            is CreateCategory    -> ByteRope.create(7) + CreateCategory.rw.serialize(it)
            is UpdateCategory    -> ByteRope.create(8) + UpdateCategory.rw.serialize(it)
            is DeleteCategory    -> ByteRope.create(9) + DeleteCategory.rw.serialize(it)
          }
        },
        persistentHashMapOf(
          0.toByte() to CreateAccount.rw,
          1.toByte() to Income.rw,
          2.toByte() to Expense.rw,
          3.toByte() to Transfer.rw,
          4.toByte() to CreateMerchant.rw,
          5.toByte() to LinkMerchants.rw,
          6.toByte() to UpdateTransaction.rw,
          7.toByte() to CreateCategory.rw,
          8.toByte() to UpdateCategory.rw,
          9.toByte() to DeleteCategory.rw
        )
      )
    }
  }
}