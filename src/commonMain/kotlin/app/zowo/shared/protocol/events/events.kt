package app.zowo.shared.protocol.events

import kotlinx.collections.immutable.PersistentList
import app.zowo.shared.lib.exts.sumByLong
import app.zowo.shared.lib.functional.*
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.lib.serialization.SerializedRW.persistentListRW
import app.zowo.shared.protocol.data.*


data class TxTimestamp(val date: LocalDate, val time: ZOption<LocalTime>) {
  companion object {
    val rw = TaggedRW2(
      0, LocalDate.rw.failIfMissing(),
      1, LocalTime.rw.noneIfMissing(),
      { date, time -> TxTimestamp(date, time) },
      { it.date }, { it.time }
    )
  }
}

data class TxCommonMeta(
  val note: ZOption<String>,
  // Is this transaction excluded from sharing?
  val isPrivate: Boolean
) {
  companion object {
    @ExperimentalStdlibApi
    val rw = TaggedRW2(
      0, SerializedRW.string.noneIfMissing(),
      1, SerializedRW.bool.failIfMissing(),
      { note, isPrivate -> TxCommonMeta(note, isPrivate) },
      { it.note }, { it.isPrivate }
    )
  }
}

data class TxData(
  val accountId: AccountId,
  val amounts: ZNonEmpty<PersistentList<CategorizedAmount>>,
  val accountType: AccountType,
  val merchant: ZOption<MerchantId>,
  val timestamp: TxTimestamp,
  val meta: TxCommonMeta
) {
  fun withReplacedCategory(from: CategoryId, to: CategoryId): TxData =
    if (amounts.value.any { it.categoryId.exists(from) }) copy(
      amounts = amounts.map {
        if (it.categoryId.exists(from)) it.copy(categoryId = ZSome(to))
        else it
      }
    )
    else this

  fun total(): Amount = Amount(amounts.value.sumByLong { it.amount.cents })

  companion object {
    @ExperimentalStdlibApi
    val rw = TaggedRW6(
      0, AccountId.rw.failIfMissing(),
      1, ZNonEmpty.rw(persistentListRW(CategorizedAmount.rw)).failIfMissing(),
      2, AccountType.rw.failIfMissing(),
      3, MerchantId.rw.noneIfMissing(),
      4, TxTimestamp.rw.failIfMissing(),
      5, TxCommonMeta.rw.failIfMissing(),
      { accountId, amounts, accountType, merchant, timestamp, meta ->
        TxData(accountId, amounts, accountType, merchant, timestamp, meta)
      },
      { it.accountId }, { it.amounts }, { it.accountType }, { it.merchant }, { it.timestamp }, { it.meta }
    )
  }
}