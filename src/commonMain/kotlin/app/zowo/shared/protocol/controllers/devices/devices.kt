package app.zowo.shared.protocol.controllers.devices

import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.protocol.data.DeviceId
import kotlin.jvm.JvmStatic

@Suppress("unused")
object Register {
  data class Response(val deviceId: DeviceId) : ISerializable, HasRw<Response> {
    override fun serialize(): ByteRope = rw.serialize(this)
    override val selfRw: ISerializedRW<Response>
      get() = rw

    companion object {
      @JvmStatic
      val rw = TaggedRW1(0, DeviceId.rw.failIfMissing(), { Response(it) }, { it.deviceId })
    }
  }
}
