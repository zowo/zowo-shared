package app.zowo.shared.protocol.controllers.sharing

import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.protocol.data.ShareId
import app.zowo.shared.protocol.data.SharingRulesetId
import app.zowo.shared.protocol.sharing.SharingRules
import kotlin.jvm.JvmStatic

object CreateOrEditRuleset {
  data class Params(val id: ZOption<SharingRulesetId>, val name: String, val rules: SharingRules) : ISerializable {
    @ExperimentalStdlibApi
    override fun serialize(): ByteRope = rw.serialize(this)

    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = TaggedRW3(
        0, SharingRulesetId.rw.noneIfMissing(),
        1, SerializedRW.string.failIfMissing(),
        2, SharingRules.rw.failIfMissing(),
        { a1, a2, a3 -> Params(a1, a2, a3) }, { it.id }, { it.name }, { it.rules }
      )
    }
  }

  data class Response(val id: SharingRulesetId) : ISerializable {
    @ExperimentalStdlibApi
    override fun serialize(): ByteRope = rw.serialize(this)

    companion object {
      @JvmStatic
      val rw = TaggedRW1(0, SharingRulesetId.rw.failIfMissing(), { Response(it) }, { it.id })
    }
  }
}

object CreateShare {
  data class Params(val name: String, val rulesetId: SharingRulesetId) : ISerializable {
    @ExperimentalStdlibApi
    override fun serialize(): ByteRope = rw.serialize(this)

    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = TaggedRW2(
        0, SerializedRW.string.failIfMissing(),
        1, SharingRulesetId.rw.failIfMissing(),
        { a1, a2 -> Params(a1, a2) }, { it.name }, { it.rulesetId }
      )
    }
  }

  data class Response(val id: ShareId) : ISerializable {
    override fun serialize(): ByteRope = rw.serialize(this)

    companion object {
      @JvmStatic
      val rw = TaggedRW1(0, ShareId.rw.failIfMissing(), { Response(it) }, { it.id })
    }
  }
}