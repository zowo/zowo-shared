package app.zowo.shared.protocol.controllers.events

import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.toPersistentList
import app.zowo.shared.app.LogEvent
import app.zowo.shared.lib.exts.contramap
import app.zowo.shared.lib.functional.ZNonEmpty
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.lib.serialization.SerializedRW.persistentListRW
import app.zowo.shared.lib.serialization.SerializedRW.persistentMapRW
import app.zowo.shared.protocol.data.DeviceId
import app.zowo.shared.protocol.data.EventRef
import app.zowo.shared.protocol.data.FirebaseUserId
import app.zowo.shared.protocol.data.LocalEventId
import app.zowo.shared.protocol.events.EventPayload
import app.zowo.shared.protocol.sharing.SharingRules
import kotlin.jvm.JvmStatic

data class DeviceEvent(val id: LocalEventId, val payload: EventPayload) : ISerializable {
  @ExperimentalStdlibApi
  override fun serialize() = rw.serialize(this)

  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = TaggedRW2(
      0, LocalEventId.rw.failIfMissing(), 1, EventPayload.rw.failIfMissing(),
      { a1, a2 -> DeviceEvent(a1, a2) }, { it.id }, { it.payload }
    )
    val comparator = LocalEventId.comparator.contramap { e: DeviceEvent -> e.id }
  }
}

data class DeviceEvents(
  val events: PersistentMap<DeviceId, ZNonEmpty<PersistentList<DeviceEvent>>>
) : ISerializable {
  @ExperimentalStdlibApi
  override fun serialize() = rw.serialize(this)

  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = TaggedRW1(
      0,
      persistentMapRW(DeviceId.rw, ZNonEmpty.rw(persistentListRW(DeviceEvent.rw)))
        .failIfMissing(),
      { DeviceEvents(it) }, { it.events }
    )
  }

  fun logEventsSorted(): ImmutableList<LogEvent> =
    events
      .flatMap { entry -> entry.value.value.map { LogEvent(EventRef(entry.key, it.id), it.payload) } }
      .sortedWith(LogEvent.comparator)
      .toPersistentList()
}

data class SharedData(val events: DeviceEvents, val rules: SharingRules) : ISerializable {
  @ExperimentalStdlibApi
  override fun serialize() = rw.serialize(this)

  companion object {
    @ExperimentalStdlibApi
    @JvmStatic
    val rw = TaggedRW2(
      0, DeviceEvents.rw.failIfMissing(),
      1, SharingRules.rw.failIfMissing(),
      { a1, a2 -> SharedData(a1, a2) }, { it.events }, { it.rules }
    )
  }
}

object Sync {
  data class Params(
    val lastKnownEvents: PersistentMap<DeviceId, LocalEventId>,
    val lastKnownOtherUserEvents: PersistentMap<FirebaseUserId, PersistentMap<DeviceId, LocalEventId>>
  ) : ISerializable {
    @ExperimentalStdlibApi
    override fun serialize() = rw.serialize(this)

    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = {
        val eventsRw = persistentMapRW(DeviceId.rw, LocalEventId.rw)
        TaggedRW2(
          0, eventsRw.failIfMissing(),
          1, persistentMapRW(FirebaseUserId.rw, eventsRw).failIfMissing(),
          { a1, a2 -> Params(a1, a2) }, { it.lastKnownEvents }, { it.lastKnownOtherUserEvents }
        )
      }()
    }
  }

  data class Response(
    // all the events from other devices that the server knows about and your device does not
    val newerEvents: DeviceEvents,
    // if local client has data for particular user and this map does not include that user anymore
    // this means that the share has been revoked and the client should delete the local data as well
    val sharedDatas: PersistentMap<FirebaseUserId, SharedData>,
    // id of the last event from your device that the server knows about
    val lastKnownEvent: ZOption<LocalEventId>
  ) : ISerializable {
    @ExperimentalStdlibApi
    override fun serialize() = rw.serialize(this)

    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = TaggedRW3(
        0, DeviceEvents.rw.failIfMissing(),
        1, persistentMapRW(FirebaseUserId.rw, SharedData.rw).failIfMissing(),
        2, LocalEventId.rw.noneIfMissing(),
        { a1, a2, a3 -> Response(a1, a2, a3) }, { it.newerEvents }, { it.sharedDatas }, { it.lastKnownEvent }
      )
    }
  }
}

object Store {
  data class Params(val events: PersistentMap<LocalEventId, EventPayload>) : ISerializable {
    @ExperimentalStdlibApi
    override fun serialize() = rw.serialize(this)

    companion object {
      @ExperimentalStdlibApi
      @JvmStatic
      val rw = TaggedRW1(
        0, persistentMapRW(LocalEventId.rw, EventPayload.rw).failIfMissing(),
        { Params(it) }, { it.events }
      )
    }
  }
}