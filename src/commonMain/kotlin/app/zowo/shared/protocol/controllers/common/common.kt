package app.zowo.shared.protocol.controllers.common

import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.serialization.*
import kotlin.jvm.JvmStatic

data class BoolResponse(val v: Boolean) : ISerializable, HasRw<BoolResponse> {
  override fun serialize(): ByteRope = rw.serialize(this)

  override val selfRw: ISerializedRW<BoolResponse>
    get() = rw

  companion object {
    @JvmStatic
    val rw = SerializedRW.bool.inmap({ BoolResponse(it) }, { it.v })
  }
}