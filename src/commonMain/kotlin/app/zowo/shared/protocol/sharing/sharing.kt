package app.zowo.shared.protocol.sharing

import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentHashMapOf
import kotlinx.collections.immutable.persistentSetOf
import app.zowo.shared.lib.data.ByteRope
import app.zowo.shared.lib.serialization.*
import app.zowo.shared.protocol.data.AccountId
import app.zowo.shared.protocol.data.CategoryId
import kotlin.jvm.JvmStatic

sealed class AccountSharingCategories {
  data class Whitelist(val ids: PersistentSet<CategoryId>) : AccountSharingCategories() {
    companion object {
      val rw = TaggedRW1(
        0, SerializedRW.persistentSetRW(CategoryId.rw).defaultIfMissing(persistentSetOf()),
        { Whitelist(it) }, { it.ids }
      )
    }
  }

  data class Blacklist(val ids: PersistentSet<CategoryId>) : AccountSharingCategories() {
    companion object {
      val rw = TaggedRW1(
        0, SerializedRW.persistentSetRW(CategoryId.rw).defaultIfMissing(persistentSetOf()),
        { Blacklist(it) }, { it.ids }
      )
    }
  }

  companion object {
    val rw by lazy {
      DiscriminatedRW(
        {
          when (it) {
            is Whitelist -> ByteRope.create(0) + Whitelist.rw.serialize(it)
            is Blacklist -> ByteRope.create(1) + Blacklist.rw.serialize(it)
          }
        },
        persistentHashMapOf(
          0.toByte() to Whitelist.rw,
          1.toByte() to Blacklist.rw
        )
      )
    }
  }
}

data class AccountSharingRules(val showBalance: Boolean, val categories: AccountSharingCategories) {
  companion object {
    val rw = TaggedRW2(
      0, SerializedRW.bool.failIfMissing(),
      1, AccountSharingCategories.rw.failIfMissing(),
      { a1, a2 -> AccountSharingRules(a1, a2) },
      { it.showBalance }, { it.categories }
    )
  }
}

sealed class SharingRules : ISerializable {
  override fun serialize(): ByteRope = rw.serialize(this)

  data class AllAccounts(val rules: AccountSharingRules) : SharingRules() {
    companion object {
      val rw = TaggedRW1(0, AccountSharingRules.rw.failIfMissing(), { AllAccounts(it) }, { it.rules })
    }
  }

  data class SelectedAccounts(val rules: PersistentMap<AccountId, AccountSharingRules>) : SharingRules() {
    companion object {
      val rw = TaggedRW1(
        0, SerializedRW.persistentMapRW(AccountId.rw, AccountSharingRules.rw).failIfMissing(),
        { SelectedAccounts(it) }, { it.rules }
      )
    }
  }

  companion object {
    @JvmStatic
    val rw by lazy {
      DiscriminatedRW(
        {
          when (it) {
            is AllAccounts -> ByteRope.create(0) + AllAccounts.rw.serialize(it)
            is SelectedAccounts -> ByteRope.create(1) + SelectedAccounts.rw.serialize(it)
          }
        },
        persistentHashMapOf(
          0.toByte() to AllAccounts.rw,
          1.toByte() to SelectedAccounts.rw
        )
      )
    }
  }
}