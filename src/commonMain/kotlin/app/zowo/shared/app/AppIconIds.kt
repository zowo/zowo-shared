package app.zowo.shared.app

import app.zowo.shared.lib.functional.ZNonEmpty
import app.zowo.shared.protocol.data.IconId
import kotlin.jvm.JvmStatic

/** Category icons that are built into the app. */
object AppIconIds {
  @JvmStatic
  val grocery = IconId(Short.MIN_VALUE)
  @JvmStatic
  val utilities = IconId((Short.MIN_VALUE + 1).toShort())
  @JvmStatic
  val fuel = IconId((Short.MIN_VALUE + 2).toShort())
  @JvmStatic
  val car = IconId((Short.MIN_VALUE + 3).toShort())
  @JvmStatic
  val health = IconId((Short.MIN_VALUE + 4).toShort())

  @Suppress("unused")
  @JvmStatic
  val all = ZNonEmpty.list(grocery, utilities, fuel, car, health)
}