package app.zowo.shared.app.state

import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZNone
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.lib.functional.ZSome
import app.zowo.shared.protocol.events.TxData
import app.zowo.shared.protocol.events.TxUpdate

fun TxData.update(upd: TxUpdate, state: State): ZEither<String, TxData> {
  return when (upd) {
    is TxUpdate.SetAccountType -> ZRight(copy(accountType = upd.v))
    is TxUpdate.SetDate -> ZRight(copy(timestamp = timestamp.copy(date = upd.v)))
    is TxUpdate.SetTime -> ZRight(copy(timestamp = timestamp.copy(time = upd.v)))
    is TxUpdate.SetIsPrivate -> ZRight(copy(meta = meta.copy(isPrivate = upd.v)))
    is TxUpdate.SetNote -> ZRight(copy(meta = meta.copy(note = upd.v)))
    is TxUpdate.SetMerchant -> when (val id = upd.v) {
      is ZNone -> ZRight(copy(merchant = id))
      is ZSome -> state.getMerchant(id.get).map { copy(merchant = id) }
    }
    is TxUpdate.SetAmounts -> ZRight(copy(amounts = upd.v))
  }
}