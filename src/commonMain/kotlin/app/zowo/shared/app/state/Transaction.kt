package app.zowo.shared.app.state

import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZLeft
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.protocol.data.CategoryId
import app.zowo.shared.protocol.data.EventRef
import app.zowo.shared.protocol.events.EventPayload
import app.zowo.shared.protocol.events.TxUpdate

sealed class Transaction {
  abstract val ref: EventRef
  abstract fun update(upd: TxUpdate, state: State): ZEither<String, Transaction>
  abstract fun withReplacedCategory(from: CategoryId, to: CategoryId): Transaction

  data class IncomeTx(override val ref: EventRef, val tx: EventPayload.Income) : Transaction() {
    override fun update(upd: TxUpdate, state: State): ZEither<String, Transaction> =
      tx.data.update(upd, state).map { copy(tx = tx.copy(data = it)) }

    override fun withReplacedCategory(from: CategoryId, to: CategoryId) =
      copy(tx = tx.copy(data = tx.data.withReplacedCategory(from, to)))
  }

  data class ExpenseTx(override val ref: EventRef, val tx: EventPayload.Expense) : Transaction() {
    override fun update(upd: TxUpdate, state: State): ZEither<String, Transaction> =
      tx.data.update(upd, state).map { copy(tx = tx.copy(data = it)) }

    override fun withReplacedCategory(from: CategoryId, to: CategoryId) =
      copy(tx = tx.copy(data = tx.data.withReplacedCategory(from, to)))
  }

  data class TransferTx(override val ref: EventRef, val tx: EventPayload.Transfer) : Transaction() {
    override fun update(upd: TxUpdate, state: State): ZEither<String, Transaction> {
      fun fail() = ZLeft("Unsupported operation $upd on $this")

      return when (upd) {
        is TxUpdate.SetDate -> ZRight(copy(tx = tx.copy(timestamp = tx.timestamp.copy(date = upd.v))))
        is TxUpdate.SetTime -> ZRight(copy(tx = tx.copy(timestamp = tx.timestamp.copy(time = upd.v))))
        is TxUpdate.SetIsPrivate -> ZRight(copy(tx = tx.copy(meta = tx.meta.copy(isPrivate = upd.v))))
        is TxUpdate.SetNote -> ZRight(copy(tx = tx.copy(meta = tx.meta.copy(note = upd.v))))
        is TxUpdate.SetAccountType -> fail()
        is TxUpdate.SetMerchant -> fail()
        is TxUpdate.SetAmounts -> fail()
      }
    }

    override fun withReplacedCategory(from: CategoryId, to: CategoryId): Transaction = this
  }
}