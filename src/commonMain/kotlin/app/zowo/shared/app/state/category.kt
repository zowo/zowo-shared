package app.zowo.shared.app.state

import app.zowo.shared.app.AppIconIds
import app.zowo.shared.lib.functional.ZNonEmpty
import app.zowo.shared.lib.functional.ZOption
import app.zowo.shared.lib.functional.ZSome
import app.zowo.shared.lib.functional.asZOption
import app.zowo.shared.protocol.data.CategoryId
import app.zowo.shared.protocol.data.CategoryName
import app.zowo.shared.protocol.data.IconId
import app.zowo.shared.protocol.events.CategoryUpdate
import kotlinx.collections.immutable.persistentMapOf
import kotlin.jvm.JvmStatic

data class Category(val icon: ZOption<IconId>, val name: CategoryName) {
  fun update(update: CategoryUpdate): Category = when (update) {
    is CategoryUpdate.SetName -> copy(name = CategoryName(update.name))
    is CategoryUpdate.SetIcon -> copy(icon = update.icon)
  }
}

data class CategoryWithId(val categoryId: CategoryId, val category: Category)

object DefaultCategories {
  val all = persistentMapOf(
          CategoryIds.grocery to Category(ZSome(AppIconIds.grocery), CategoryName("Groceries")),
          CategoryIds.fuel to Category(ZSome(AppIconIds.fuel), CategoryName("Fuel")),
          CategoryIds.car to Category(ZSome(AppIconIds.car), CategoryName("Car")),
          CategoryIds.utilities to Category(ZSome(AppIconIds.utilities), CategoryName("Utilities")),
          CategoryIds.health to Category(ZSome(AppIconIds.health), CategoryName("Health"))
  )

  fun categoryWithId(categoryId: CategoryId): ZOption<CategoryWithId> = all[categoryId]?.let { CategoryWithId(categoryId, it) }.asZOption()
}

object CategoryIds {
  @JvmStatic
  val grocery = CategoryId.of(Long.MIN_VALUE + 0)
  @JvmStatic
  val utilities = CategoryId.of(Long.MIN_VALUE + 1)
  @JvmStatic
  val fuel = CategoryId.of(Long.MIN_VALUE + 2)
  @JvmStatic
  val car = CategoryId.of(Long.MIN_VALUE + 3)
  @JvmStatic
  val health = CategoryId.of(Long.MIN_VALUE + 4)

  @Suppress("unused")
  @JvmStatic
  val all = ZNonEmpty.list(grocery, utilities, fuel, car, health)
}