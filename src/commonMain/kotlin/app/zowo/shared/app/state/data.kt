package app.zowo.shared.app.state

import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.PersistentMap
import app.zowo.shared.lib.exts.indexWhere
import app.zowo.shared.lib.functional.ZEither
import app.zowo.shared.lib.functional.ZLeft
import app.zowo.shared.lib.functional.ZRight
import app.zowo.shared.protocol.data.*

data class TransferTxLink(val from: AccountId, val to: AccountId)

data class ProfileSettings(
  val userName: String,
  val primaryCurrency: Currency
)

data class Account(
  val accountDetails: AccountDetails,
  val transactions: PersistentList<Transaction>
) {
  fun addTx(transaction: Transaction, amount: Amount): Account = copy(
    accountDetails = accountDetails + amount,
    transactions = transactions.add(transaction)
  )

  fun modTx(ref: EventRef, f: (Transaction) -> ZEither<String, Transaction>): ZEither<String, Account> =
    when (val res = transactions.indexWhere { it.ref == ref }) {
      null -> ZLeft("Can't find transaction by $ref")
      else -> f(res.second).map { newTx -> copy(transactions = transactions.set(res.first, newTx)) }
    }
}

data class AccountDetails(
  val name: AccountName,
  val owner: AccountOwner,
  val currency: Currency,
  val accountType: AccountType,
  val balance: Amount
) {
  operator fun plus(amount: Amount): AccountDetails = copy(balance = balance + amount)
  operator fun minus(amount: Amount): AccountDetails = copy(balance = balance - amount)
}

sealed class AccountOwner {
  object Me : AccountOwner()
  data class OtherUser(val userId: FirebaseUserId, val name: String) : AccountOwner()
}

sealed class Merchant {
  fun resolve(merchants: PersistentMap<MerchantId, Merchant>): ZEither<String, Real> =
    when (this) {
      is Link -> merchants[id]?.resolve(merchants) ?: ZLeft("Can't find merchant by $id")
      is Real -> ZRight(this)
    }

  data class Link(val id: MerchantId) : Merchant()
  data class Real(val name: String) : Merchant()
}