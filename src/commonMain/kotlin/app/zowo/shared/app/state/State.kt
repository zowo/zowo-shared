package app.zowo.shared.app.state

import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentListOf
import app.zowo.shared.app.LogEvent
import app.zowo.shared.lib.compat.Empty
import app.zowo.shared.lib.exts.*
import app.zowo.shared.lib.functional.*
import app.zowo.shared.protocol.data.*
import app.zowo.shared.protocol.events.CategoryUpdate
import app.zowo.shared.protocol.events.EventPayload
import app.zowo.shared.protocol.events.TxUpdate

data class State(
  val accounts: PersistentMap<AccountId, Account>,
  val categories: PersistentMap<CategoryId, Category>,
  val merchants: PersistentMap<MerchantId, Merchant>,
  val txLinks: PersistentMap<EventRef, AccountId>,
  val transferTxLinks: PersistentMap<EventRef, TransferTxLink>,
  val profileSettings: ProfileSettings,
  val maxLocalEventId: ZOption<LocalEventId>
) {
  companion object {
    val empty = State(
      Empty.map(),
      DefaultCategories.all,
      Empty.map(),
      Empty.map(),
      Empty.map(),
      ProfileSettings("Undefined", Currency("EUR")),
      ZNone
    )

    fun <A> newId(createRandom: () -> A, exists: (A) -> Boolean): A {
      var current = createRandom()
      while (exists(current)) current = createRandom()
      return current
    }
  }

  val nextLocalEventId: LocalEventId
    get() = maxLocalEventId.fold(LocalEventId.MIN, { it.next })

  fun getMerchant(id: MerchantId): ZEither<String, Merchant> = when (val m = merchants[id]) {
    null -> ZLeft("Unknown merchant $id")
    else -> ZRight(m)
  }

  fun modAccount(id: AccountId, f: (Account) -> Account): ZEither<String, State> {
    val maybeAccount = accounts.get(id)
    return (
      if (maybeAccount != null) ZRight(copy(accounts = accounts.put(id, f(maybeAccount))))
      else ZLeft("Can't find account by $id")
      )
  }

  fun putMerchant(id: MerchantId, m: Merchant): ZEither<String, State> =
    when (val maybeMerchant = merchants[id]) {
      null -> ZRight(copy(merchants = merchants.put(id, m)))
      else -> ZLeft("Merchant $id already exists as $maybeMerchant, tried to put $m")
    }

  fun linkMerchants(link: EventPayload.LinkMerchants): ZEither<String, State> {
    if (!merchants.containsKey(link.from)) return ZLeft("Can't $link, no source merchant")
    return when (merchants[link.to]) {
      null -> ZLeft("Can't $link, no target merchant")
      else -> ZRight(copy(merchants = merchants.put(link.from, Merchant.Link(link.to))))
    }
  }

  fun modTx(ref: EventRef, updates: Iterable<TxUpdate>): ZEither<String, State> {
    return when (val accountId = txLinks[ref]) {
      null -> ZLeft("Unknown transaction $ref")
      else -> when (val account = accounts[accountId]) {
        null ->
          ZLeft("Consistency error, can't find account by $accountId")
        else ->
          account.modTx(ref) { updates.foldLeftEither(it) { tx, upd -> tx.update(upd, this) } }
            .map { copy(accounts = accounts.put(accountId, it)) }
      }
    }
  }

  fun addTxLink(ref: EventRef, id: AccountId): ZEither<String, State> =
    when (val target = txLinks[ref]) {
      null -> ZRight(copy(txLinks = txLinks.put(ref, id)))
      else -> ZLeft("Can't link $ref to $id, it already points to $target")
    }

  fun addTransferTxLink(ref: EventRef, link: TransferTxLink): ZEither<String, State> =
    when (val target = transferTxLinks[ref]) {
      null -> ZRight(copy(transferTxLinks = transferTxLinks.put(ref, link)))
      else -> ZLeft("Can't link $ref to $link, it already points to $target")
    }

  fun addTx(ref: EventRef, value: EventPayload.Income): ZEither<String, State> =
    modAccount(value.data.accountId) { acc -> acc.addTx(Transaction.IncomeTx(ref, value), value.data.total()) }
      .flatMap { it.addTxLink(ref, value.data.accountId) }

  fun addTx(ref: EventRef, value: EventPayload.Expense): ZEither<String, State> =
    modAccount(value.data.accountId) { acc -> acc.addTx(Transaction.ExpenseTx(ref, value), -value.data.total()) }
      .flatMap { it.addTxLink(ref, value.data.accountId) }

  fun addTx(ref: EventRef, value: Transaction.TransferTx): ZEither<String, State> =
    modAccount(value.tx.from.id) { acc -> acc.addTx(value, -value.tx.from.amount) }
      .flatMap { it.modAccount(value.tx.to.id) { acc -> acc.addTx(value, value.tx.to.amount) } }
      .flatMap { it.addTransferTxLink(ref, TransferTxLink(value.tx.from.id, value.tx.to.id)) }

  fun addAccount(id: AccountId, account: Account): ZEither<String, State> =
    when (val existing = accounts[id]) {
      null -> ZRight(copy(accounts = accounts.put(id, account)))
      else -> ZLeft("Wanted to write $account to $id, but $existing is already there")
    }

  operator fun get(id: AccountId): ZEither<String, Account> =
    when (val account = accounts[id]) {
      null -> ZLeft("Unknown account $id")
      else -> ZRight(account)
    }

  operator fun get(id: CategoryId): ZEither<String, Category> =
    when (val category = categories[id]) {
      null -> ZLeft("Unknown category $id")
      else -> ZRight(category)
    }

  operator fun get(id: MerchantId): ZEither<String, Merchant> =
    when (val merchant = merchants[id]) {
      null -> ZLeft("Unknown merchant $id")
      else -> ZRight(merchant)
    }

  @Suppress("unused")
  fun newAccountId(): AccountId = newId({ AccountId.random() }, { accounts.containsKey(it) })
  @Suppress("unused")
  fun newCategoryId(): CategoryId = newId({ CategoryId.random() }, { categories.containsKey(it) })
  @Suppress("unused")
  fun newMerchantId(): MerchantId = newId({ MerchantId.random() }, { merchants.containsKey(it) })

  fun createCategory(id: CategoryId, category: Category): ZEither<String, State> =
    when (val existing = categories[id]) {
      null -> ZRight(copy(categories = categories.put(id, category)))
      else -> ZLeft("Wanted to create $category with $id, but $existing is already there")
    }

  fun modCategory(id: CategoryId, updates: Iterable<CategoryUpdate>): ZEither<String, State> =
    this[id].map { copy(categories = categories.put(id, updates.fold(it) { cat, upd -> cat.update(upd) })) }

  fun deleteCategory(id: CategoryId, transferTo: CategoryId): ZEither<String, State> =
    this[id].flatMap {
      this[transferTo].map {
        copy(
          categories = categories.remove(id),
          accounts = accounts.mapValues { _, acc ->
            acc.copy(
              transactions = acc.transactions.map { it.withReplacedCategory(id, transferTo) }
            )
          }
        )
      }
    }

  fun update(event: LogEvent): ZEither<String, State> {
    val either = when (val data = event.data) {
      is EventPayload.CreateAccount -> addAccount(
        data.id,
        Account(
          AccountDetails(
            data.name,
            //TODO account owner how to determine?
            AccountOwner.Me,
            data.currency,
            data.type,
            data.initial
          ),
          persistentListOf()
        )
      )
      is EventPayload.Income -> addTx(event.ref, data)
      is EventPayload.Expense -> addTx(event.ref, data)
      is EventPayload.Transfer -> addTx(event.ref, Transaction.TransferTx(event.ref, data))
      is EventPayload.CreateMerchant -> putMerchant(data.id, Merchant.Real(data.name))
      is EventPayload.LinkMerchants -> linkMerchants(data)
      is EventPayload.UpdateTransaction -> modTx(data.ref, data.updates.value)
      is EventPayload.CreateCategory -> createCategory(data.id, Category(data.icon, data.name))
      is EventPayload.UpdateCategory -> modCategory(data.id, data.updates.value)
      is EventPayload.DeleteCategory -> deleteCategory(data.id, data.transferTo)
    }

    val isNewerEvent = maxLocalEventId.exists(event.ref.localEventId, { e, v -> v > e })
    return if (isNewerEvent) either.map { it.copy(maxLocalEventId = ZSome(event.ref.localEventId)) } else either
  }

  @Suppress("unused")
  fun updateAll(events: Iterable<LogEvent>): FoldEitherResult<LogEvent, String, State> =
    events.foldLeftEitherErrors(this) { lastState, event -> lastState.update(event) }
}