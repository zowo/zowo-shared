package app.zowo.shared.app

import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.PersistentMap
import app.zowo.shared.lib.exts.contramap
import app.zowo.shared.protocol.data.EventRef
import app.zowo.shared.protocol.data.FirebaseUserId
import app.zowo.shared.protocol.events.EventPayload
import kotlin.jvm.JvmStatic

data class LogEvent(
  val ref: EventRef, val data: EventPayload
) : Comparable<LogEvent> {
  companion object {
    @JvmStatic
    val comparator = EventRef.comparator.contramap { e: LogEvent -> e.ref }
  }

  override fun compareTo(other: LogEvent): Int = comparator.compare(this, other)
}

data class EventLog(val events: PersistentList<LogEvent>)
data class EventLogs(val logs: PersistentMap<FirebaseUserId, EventLog>)