## Publishing JVM library

Run `./publishToLocal.sh`

## IOS
Build for XCode (initially):

`./gradlew packForXcode`

Then read [How to use this in XCode project](https://play.kotlinlang.org/hands-on/Targeting%20iOS%20and%20Android%20with%20Kotlin%20Multiplatform/06_SettingUpKotlinFramework).

Further reading:
* [Kotlin/ObjC interop](https://kotlinlang.org/docs/reference/native/objc_interop.html)
